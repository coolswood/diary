Feature('Тестирование_пинкода');

Before((I) => {
    I.amOnPage('/');
});

const enterPin = (I) => {
    I.click('1');
    I.click('2');
    I.click('3');
    I.click('4');
};

const successEnter = (I) => {
    I.click('Меню');
    I.click('Установить пароль');
    I.click("button a[href='/login/add']");

    enterPin(I);

    enterPin(I);

    I.see('Главная');
};

Scenario('Успешкая установка пароля и успешный вход', (I) => {

    successEnter(I);

    I.refreshPage();

    enterPin(I);

    I.see('Главная');

});

Scenario('Пароль не совпадает с подтверждением', (I) => {

    I.click('Меню');
    I.click('Установить пароль');
    I.click("button a[href='/login/add']");

    enterPin(I);

    I.click('1');
    I.click('2');
    I.click('3');
    I.click('5');

    I.dontSee('Главная');

    I.wait(1);

    I.click('1');
    I.click('2');
    I.click('3');

    I.click(`.pincode__actions[data-test='delete']`);
    I.click(`.pincode__actions[data-test='delete']`);
    I.click(`.pincode__actions[data-test='delete']`);

    I.dontSee('Главная');

    enterPin(I);

    I.see('Главная');

});

Scenario('Сброс пароля', (I) => {

    successEnter(I);

    I.click('Меню');
    I.click('Установить пароль');
    I.click('Сбросить пароль');

    I.refreshPage();

    I.see('Пароль');

});

Scenario('Забыл пароль', (I) => {

    successEnter(I);

    I.refreshPage();

    I.click('Сброс');
    I.click('Вернуться');
    I.click('Сброс');
    I.click('Сбросить');

    I.refreshPage();

    I.see('Главная');

});
