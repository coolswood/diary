Feature('Тестирование_раздела_дневник_мыслей');

Before((I) => {
    I.amOnPage('/');
});

Scenario('Создаю новую автоматическую мысль', (I) => {

    I.click('#diary-list');
    I.click('button[aria-label=Add]');

    I.fillField('Событие / ситуация / триггер', 'test');
    I.fillField('Автоматические мысли', 'test');

    //
    I.click('.logic-error .errors-checkbox__select');

    I.click('Обобщение');
    I.see('Обобщение', {css: '.logic-error .errors-checkbox__item--active'});

    I.click('Обобщение');
    I.dontSee('.errors-checkbox__item--active');

    I.click('.logic-error .errors-checkbox__select');

    //
    I.click('.emotion .errors-checkbox__select');

    I.click('Печаль');
    I.see('Печаль', {css: '.emotion .errors-checkbox__item--active'});

    I.click('Печаль');
    I.dontSee('.errors-checkbox__item--active');

    I.click('.emotion .errors-checkbox__select');

    //
    I.fillField('#range-input__0', '3');
    I.fillField('Адекватная оценка', 'test');
    I.fillField('Вывод', 'test');

    I.click('button[aria-label=Add]');

    I.seeElement('.card');
});

Scenario('Просматриваю автоматическую мысль', (I) => {

    I.click('#diary-list');
    I.click('.card');

    I.seeNumberOfElements('.card', 4);
});

Scenario('Редактирую автоматическую мысль', (I) => {

    I.click('#diary-list');
    I.click('.card');

    I.click('button[aria-label=Add]');

    I.fillField('Событие / ситуация / триггер', '111');

    I.click('button[aria-label=Add]');

    I.seeNumberOfElements('.card', 4);
});

Scenario('Удаляю автоматическую мысль', (I) => {

    I.click('#diary-list');
    I.click('.card');

    I.click('button[aria-label=Add]');

    I.fillField('Событие / ситуация / триггер', '111');

    I.click('button[aria-label=Add]');

    I.seeNumberOfElements('.card', 4);
});