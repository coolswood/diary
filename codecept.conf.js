const {setHeadlessWhen} = require('@codeceptjs/configure');

setHeadlessWhen(process.env.HEADLESS);

exports.config = {
    tests: './codecept/tests/**/*.js',
    output: './codecept/output',
    helpers: {
        Puppeteer: {
            url: 'http://localhost:3000',
            show: false,
            windowSize: '375x667',
            restart: false
        }
    },
    include: {
        I: './codecept/steps_file.js'
    },
    bootstrap: null,
    mocha: {},
    name: 'diary',
    translation: 'ru-RU',
    plugins: {
        retryFailedStep: {
            enabled: true
        },
        screenshotOnFail: {
            enabled: true
        }
    }
}