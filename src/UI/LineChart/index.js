import React, {Component} from 'react';

import ReactEchartsCore from 'echarts-for-react/lib/core';
import echarts from 'echarts/lib/echarts';
import 'echarts/lib/chart/line';
import 'echarts/lib/component/visualMap';


import {COLOR_EXTRIM, COLOR_HARD, COLOR_LIGHT, COLOR_MIDDLE, COLOR_NORMAL} from "constants/index";

export default class LineChart extends Component {
    state = {
        options: {}
    };

    componentDidMount() {
        this.setState({
            options: this.getOption()
        })
    }

    getOption = () => {
        const { data } = this.props;

        return {
            xAxis: {
                type: 'category',
                axisLine: {
                    show: false
                },
                axisTick: {
                    show: false
                },
            },
            yAxis: {
                type: 'value',
                axisLine: {
                    show: false
                },
                axisTick: {
                    show: false
                }
            },
            grid: {
                top: 20,
                bottom: 20,
                right: 0,
                left: 23
            },
            visualMap: {
                show: false,
                pieces: [{
                    gt: -1,
                    lte: 10,
                    color: COLOR_NORMAL
                }, {
                    gt: 10,
                    lte: 16,
                    color: COLOR_LIGHT
                }, {
                    gt: 16,
                    lte: 20,
                    color: COLOR_MIDDLE
                }, {
                    gt: 20,
                    lte: 30,
                    color: COLOR_HARD
                }, {
                    gt: 30,
                    color: COLOR_EXTRIM
                }]
            },
            series: [{
                data: data,
                type: 'line',
                smooth: true,
                symbolSize: 6
            }]
        }
    };

    render() {
        return (
            <ReactEchartsCore
                echarts={echarts}
                option={this.state.options}
                style={{height: '200px'}}
                notMerge={true}
            />
        )
    }
}