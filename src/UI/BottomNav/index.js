import React from 'react';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import HomeIcon from '@material-ui/icons/Home';
import BookIcon from '@material-ui/icons/Book';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';

import './stile.sass'

export default class SimpleBottomNavigation extends React.Component {

    handleChange = (event, value) => {
        this.props.history.push({
            pathname: value
        })
    };

    render() {
        const { value } = this.props;

        return (
            <div className='bottom-nav'>
                <BottomNavigation
                    value={value}
                    onChange={this.handleChange}
                >
                    <BottomNavigationAction label="Главная" value='/' icon={<HomeIcon />} />
                    <BottomNavigationAction label="Чтение" value='/books' icon={<BookIcon />} />
                    <BottomNavigationAction label="Статистика" value='/stats' icon={<TrendingUpIcon />} />
                </BottomNavigation>
            </div>
        );
    }
}