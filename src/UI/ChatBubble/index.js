import React from 'react';

import './stale.sass'

export default (props) => (
    <div className="chat">
        {props.chat.map(item => (
            <div key={item} className={`${item.type} messages`}>
                {item.messages.map(i => (
                    <div key={i} className="message">
                        {i}
                    </div>
                ))}
            </div>
        ))}
    </div>
)