import React from 'react';
import { Link } from 'react-router-dom';

import './style.sass';

export default (props) => (
    props.to ? <Link to={props.to} {...props} style={props.style} className='label'>
        {props.children}
    </Link> : <div {...props} style={props.style} className='label'>
        {props.children}
    </div>
)