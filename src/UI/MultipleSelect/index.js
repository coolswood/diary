import React, {Component} from 'react';
import { Link } from 'react-router-dom';

import Collapse from '@material-ui/core/Collapse';
import ListItem from '@material-ui/core/ListItem';
import InfoIcon from '@material-ui/icons/Info';
import ListItemText from '@material-ui/core/ListItemText';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

import './style.sass'

export default class ErrorsCheckbox extends Component {

    state = {
        open: false
    };

    handleClick = () => {
        this.setState(state => ({ open: !state.open }));
    };

    render() {
        const { selected, select, className, title, info = false, content } = this.props;

        return (
            <div className={`errors-checkbox ${className}`}>
                <div className="info__wrap">
                    <div className="errors-checkbox__select">
                        <ListItem button={true} onClick={this.handleClick}>
                            <ListItemText primary={title} />
                            {this.state.open ? <ExpandLess /> : <ExpandMore />}
                        </ListItem>
                    </div>
                    {info && <Link className = 'info' to={{
                        pathname: '/info',
                        context: info
                    }}><InfoIcon /></Link>}
                </div>
            <Collapse in={this.state.open} timeout="auto">
                {content.map((item, i) => {
                    let isActive = selected.indexOf(item.id) !== -1;
                    return (
                        <button type="button" onClick={() => select(item.id)} key={i} className={`errors-checkbox__item ${isActive ? 'errors-checkbox__item--active' : ''}`}>
                            <>
                                {!item.moji && <img className='logic-error-item' src={require(`../../img/errors/${item.id}.ico`)} alt="img" />}
                                {!!item.moji && <span className='logic-error-item'>{item.moji}</span>}
                                {item.text}
                            </>
                        </button>
                    )
                })}
            </Collapse>
        </div>
        )
    }
}