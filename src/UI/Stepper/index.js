import React, {Component} from 'react';
import MobileStepper from '@material-ui/core/MobileStepper';
import { withStyles } from '@material-ui/core/styles';

import './style.sass'

const styles = {
    root: {
        flexGrow: 1,
        background: 'none'
    }
};

class ProgressMobileStepper extends Component {
    render() {
        const { classes, steps, activeStep } = this.props;

        return(
            <div className='stepper'>
                <MobileStepper
                    variant="progress"
                    steps={steps}
                    position="static"
                    activeStep={activeStep}
                    className={classes.root}
                />
            </div>
        )
    }
}

export default withStyles(styles, { withTheme: true })(ProgressMobileStepper);