import React, {Component} from 'react';

import Button from '@material-ui/core/Button';

export default class Home extends Component {
    render() {
        const { children, color = "primary" } = this.props;

        return (
            <div className='button'>
                <Button
                    variant="contained"
                    color={color}
                    style={{
                        width: '100%',
                        marginBottom: 10
                    }}
                    type='submit'
                    {...this.props}
                >
                    {children}
                </Button>
            </div>
        )
    }
}