import React, {Component} from 'react';

import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';

import { Link } from 'react-router-dom';

import './style.sass'

export default class Menu extends Component {

    render() {
        const {toggleMenu, openedMenu} = this.props;

        return (
            <>
                <div onClick={toggleMenu} className={`menu-info-filtered ${openedMenu ? 'menu-info-filtered--active' : ''}`} />
                <div className={`menu-info ${openedMenu ? 'menu-info--active' : ''}`}>
                    <div className="menu-info__close">
                        <IconButton aria-label="Delete" size="medium">
                            <CloseIcon onClick={toggleMenu} />
                        </IconButton>
                    </div>
                    <div className="menu-info__nav">
                        <h3 className='menu-info__h'>Меню</h3>
                        <Link to='/sync' className="menu-info__item">Синхронизация</Link>
                        <Link to='/settings' className="menu-info__item">Настройки</Link>
                        <Link to='/policy' className="menu-info__item">Конфиденциальность</Link>
                        <Link to='/login' className="menu-info__item">Установить пароль</Link>
                        <h3 className='menu-info__h'>Разработчики</h3>
                        <a href='https://vk.com/in_vino_veritas_1' className="menu-info__item">Даниил Сугоняев</a>
                        <a href='https://www.instagram.com/sophe_vl/' className="menu-info__item">Софья Власова</a>
                    </div>
                </div>
            </>
        )
    }
}
