import React, {Component} from "react";
import Collapse from '@material-ui/core/Collapse';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from "@material-ui/core/ListItem";
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

import ProxyIcons from 'UI/ProxyIcons';

import {HDU_ERRORS} from "constants/HDU";

export default class HDUErrorsList extends Component {

    state = {
        opened: ''
    };

    handleClick = (id) => {
        this.setState({
            opened: id === this.state.opened ? '' : id
        })
    };

    render() {
        return (
            HDU_ERRORS.map(item => {
                return (
                    <div key={item.id} style={{marginBottom: 10}} className='errors-checkbox__select'>
                        <ListItem button onClick={() => this.handleClick(item.id)}>
                            <ProxyIcons
                                src={item.id}
                                className='logic-error-item'
                            />
                            <ListItemText primary={item.text} />
                            {this.state.open ? <ExpandLess /> : <ExpandMore />}
                        </ListItem>
                        <Collapse in={this.state.opened === item.id} timeout="auto">
                            <div>
                                {item.disc}
                            </div>
                        </Collapse>
                    </div>
                )
            })
        )
    }
}