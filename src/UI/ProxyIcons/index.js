import React from "react";

import common from "img/errors/common.ico";
import discval from "img/errors/discval.ico";
import emo from "img/errors/emo.ico";
import filter from "img/errors/filter.ico";
import jump from "img/errors/jump.ico";
import label from "img/errors/label.ico";
import max from "img/errors/max.ico";
import maybe from "img/errors/maybe.ico";
import moreMine from "img/errors/moreMine.ico";
import response from "img/errors/response.ico";

import alone from "img/HDU/alone.ico";
import good from "img/HDU/good.ico";
import lable from "img/HDU/lable.ico";
import love from "img/HDU/love.ico";
import power from "img/HDU/power.ico";
import supers from "img/HDU/supers.ico";
import work from "img/HDU/work.ico";

import graphImg from 'img/non/nonGraph.svg'
import emptyImg from 'img/non/empty.svg'

const IMAGES = {
    common,
    discval,
    emo,
    filter,
    jump,
    label,
    max,
    maybe,
    moreMine,
    response,
    alone,
    good,
    lable,
    love,
    power,
    supers,
    work,
    graphImg,
    emptyImg
};

export const preloadImg = () => {
    return Object.keys(IMAGES).map(i => (
        <img style={{display: 'none'}} src={IMAGES[i]} key={i} alt='error' />
    ))
}

export default ({src, className, style}) => {
    return <img rel="preload" style={style} className={className} src={IMAGES[src]} key={src} alt={src} />
}