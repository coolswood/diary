import React from "react";
import {Link} from "react-router-dom";
import IconStar from '@material-ui/icons/Stars';
import {VERSION, COLOR_NORMAL} from "../../../../constants";
import InfoIcon from '@material-ui/icons/Info';
import SyncIcon from '@material-ui/icons/Cached';
import {getLocalStore} from "instruments";

export default ({newVersion, info}) => {
    const isNewVersion = localStorage.getItem('version') !== VERSION;
    const authorization = getLocalStore('authorization');

    return (
        <div className="header__info">
            <Link to={{
                pathname: '/sync'
            }}><SyncIcon style={{color: authorization ? COLOR_NORMAL : 'black'}} /></Link>

            {(newVersion && isNewVersion) &&<Link to={{
                pathname: '/info',
                context: newVersion
            }}><IconStar
                color='primary'
            /></Link>}

            {info && <Link to={{
                pathname: '/info',
                context: info
            }}><InfoIcon /></Link>}
        </div>
    )
}