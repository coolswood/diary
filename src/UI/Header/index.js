import React, {Component} from 'react';
import './style.sass';

import IconButton from '@material-ui/core/IconButton';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import MenuIcon from '@material-ui/icons/Menu';
import ym from 'react-yandex-metrika';
import {preloadImg} from 'UI/ProxyIcons'

import HeaderInfo from './components/HeaderInfo'

import Menu from '../Menu';

export default class Header extends Component {

    constructor(props) {
        super(props);

        this.route = '';
    }

    state = {
        openedMenu: false
    };

    toggleMenu = () => {
      this.setState({
          openedMenu: !this.state.openedMenu
      })
    };

    render() {
        const {h1, history, location, back = true, menu = false, info = false, newVersion = false} = this.props;
        const {openedMenu} = this.state;

        if (location.pathname !== this.route) {

            if(!!window.Ya) {
                ym('hit', location.pathname);
            }

            window.scrollTo(0, 0);
            this.route = location.pathname;
        }

        return (
            <div className="header-placeholder">
                {preloadImg()}
                <Menu
                    openedMenu={openedMenu}
                    toggleMenu={this.toggleMenu}
                />
                <div className='header'>
                    {back && <div className="header__back" onClick={() => typeof(back) === 'boolean' ? history.goBack() : history.push(back)}>
                        <IconButton aria-label="back" size="medium">
                            <ArrowDownwardIcon fontSize="inherit" />
                        </IconButton>
                    </div>}
                    {menu && <IconButton onClick={this.toggleMenu} aria-label="menu" size="medium">
                        <span className='visualy-hidden'>Меню</span>
                        <MenuIcon fontSize="inherit" />
                    </IconButton>}
                    <div className="header__text">{h1}</div>
                    <HeaderInfo
                        newVersion={newVersion}
                        info={info}
                    />
                </div>
            </div>
        )
    }
}
