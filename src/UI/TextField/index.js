import React, {Component} from 'react';

import TextField from '@material-ui/core/TextField';
import Message from "../Message"

import './style.sass';

export default class Text extends Component {

    tipeText = (text) => {
        const { tipeText, type } = this.props;
        tipeText(type, text.target.value)
    };

    render() {
        const { label, value, error, className, errorMessage, style } = this.props;

        return (
            <div className={`text-field ${className}`}>
                <TextField
                    className={`text-item ${error && 'text-item--error'}`}
                    label={label}
                    name={label}
                    multiline
                    rowsMax="10"
                    value={value}
                    onChange={this.tipeText}
                    margin="normal"
                    style={{
                        width: '100%',
                        ...style
                    }}
                    variant="outlined"
                />
                {errorMessage && <Message type="danger">{errorMessage}</Message>}
            </div>
        )
    }
}