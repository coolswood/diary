import React, {Component} from 'react';

import MobileStepper from '@material-ui/core/MobileStepper';
import Button from '@material-ui/core/Button';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';

// import './style.sass';

export default class Carousel extends Component {

    handleNext = () => {
        this.props.handleCarousel(this.props.step + 1)
    };

    handleBack = () => {
        this.props.handleCarousel(this.props.step - 1)
    };

    render() {
        const { step, steps } = this.props;

        return (
            <div className="carousel">
                <MobileStepper
                    variant="dots"
                    steps={steps}
                    position="static"
                    activeStep={step}
                    nextButton={
                        <Button onClick={this.handleNext} size="small" disabled={step === steps - 1}>
                            Вперед
                            <KeyboardArrowRight />
                        </Button>
                    }
                    backButton={
                        <Button onClick={this.handleBack} size="small" disabled={step === 0}>
                            <KeyboardArrowLeft />
                            Назад
                        </Button>
                    }
                />
            </div>
        )
    }
}