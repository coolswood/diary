import React from 'react';

import './style.sass';

export default (props) => (
    <div style={props.style} className={`card ${props.className ? props.className : ''}`}>
        {props.children}
    </div>
)