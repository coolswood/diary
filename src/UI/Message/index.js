import React from 'react';
import "./style.sass"

export default ({type, children}) => {
    return (
        <div className={`message message--${type}`}>
            {children}
        </div>
    )
}