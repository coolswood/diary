import React, {Component} from 'react';
import { YMInitializer } from 'react-yandex-metrika';
import './style.sass';
import {isProd} from "instruments";

export default class Home extends Component {

    render() {
        const { children } = this.props;

        return (
            <>
                <div className="page-bg" />
                <div className='page'>
                    {children}
                    {isProd() && <YMInitializer accounts={[54644557]} />}
                </div>
            </>
        )
    }
}