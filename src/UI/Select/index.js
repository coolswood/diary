import React, {Component} from 'react';

import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

import './style.sass'

export default class SelectComponent extends Component {

    tipeText = (text) => {
        const { tipeText, type } = this.props;
        tipeText(type, text.target.value)
    };

    render() {
        const { value, items } = this.props;

        return (
            <div className='select'>
                <FormControl>
                    <Select
                        value={value}
                        onChange={this.tipeText}
                        input={<Input name="age" id="age-helper" />}
                    >
                        {items.map(item => (
                            <MenuItem value={item}>{item}</MenuItem>
                        ))}
                    </Select>
                </FormControl>
            </div>
        )
    }
}