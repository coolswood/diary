import React, {useState} from 'react';

import Card from '../Card';

import './style.sass';

import {mood} from '../../constants/diary';

export default (props) => {

    const setValue = (e) => {
        props.changeRange(e.target.value);
    };

    return (
        <div className={`range-mood ${props.className ? props.className : ''}`}>
            <Card className='card--middle'>
                <div style={{color: 'rgba(0, 0, 0, 0.87)', marginBottom: 5}}>Уровень эмоционального расстройства</div>
                <input type="range" min='0' max='5' value={props.value} id={`range-input__${props.value}`} onChange={setValue} />
                {/*<div>{mood[props.value].name}</div>*/}
            </Card>
        </div>
    )
}

// 0 30 130 170