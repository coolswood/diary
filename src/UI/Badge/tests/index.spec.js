import React from 'react';
import Badge from '../index';

import {shallow} from 'enzyme';

describe('Badge', () => {

    it('render empty', () => {
        const element = shallow(<Badge />).html();
        expect(element).toMatchSnapshot()
    });

    it('render with className', () => {
        const props = {
            className: 'test test-class'
        };

        const element = shallow(<Badge {...props} />).html();
        expect(element).toMatchSnapshot()
    });

    it('render with size', () => {
        const props = {
            size: 'small'
        };

        const element = shallow(<Badge {...props} />).html();
        expect(element).toMatchSnapshot()
    });

    it('render with style', () => {
        const props = {
            style: {color: 'red'}
        };

        const element = shallow(<Badge {...props} />).html();
        expect(element).toMatchSnapshot()
    });

    it('render with children', () => {
        const props = {
            children: 123
        };

        const element = shallow(<Badge {...props} />).html();
        expect(element).toMatchSnapshot()
    });
});