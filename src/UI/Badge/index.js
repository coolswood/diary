import React from 'react';

import './style.sass'

export default (props) => {
    return (
        <div
            className={`badge badge--${props.size || 'normal'} ${props.className || ''}`}
            style={props.style}
        >{props.children}</div>
    )
}