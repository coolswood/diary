import React from 'react';

import Card from 'UI/Card';
import {Link} from "react-router-dom";

import './style.sass';

const render = (props) => (
    <Card className='card-new-version'>
        {props.children}
        {!!props.link && <span className="read-more">Посмотреть →</span>}
    </Card>
);

export default (props) => props.link ? (
    <Link to={props.link}>
        {render(props)}
    </Link>
) : (
    render(props)
)