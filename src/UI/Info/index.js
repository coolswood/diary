import React from 'react';

import Page from '../../UI/Page';
import Header from '../../UI/Header';

export default (props) => {
    const { context } = props.location;
    return (
        <>
            <Header
                h1='Информаторий'
                back={true}
                {...props}
            />
            <Page>
                {context}
            </Page>
        </>
    )
}