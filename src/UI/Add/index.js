import React, {Component} from 'react';
import Fab from '@material-ui/core/Fab';

import './style.sass';

export default class Add extends Component {
    render() {
        const { className, children } = this.props;

        return (
            <div
                className={`add ${className ? className : ''}`}
                {...this.props}
            >
                <Fab color="primary" aria-label="Add">
                    {children}
                </Fab>
            </div>
        )
    }
}