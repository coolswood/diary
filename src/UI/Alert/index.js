import React from 'react';
import ProxyIcons from 'UI/ProxyIcons';

import './style.sass'

const TYPES = {
    graph: {
        img: 'graphImg',
        text: 'Недостаточно данных для отображения графика'
    },
    empty: {
        img: 'emptyImg',
        text: 'Здесь пока нет ни одной записи'
    },
};

export default ({type}) => {
    return (
        <div className='alert'>
            <ProxyIcons
                style={{width: type === 'graph' ? '70%' : '50%'}}
                className='alert__img'
                src={TYPES[type].img}
            />
            <span className='alert__text'>{TYPES[type].text}</span>
        </div>
    )
}