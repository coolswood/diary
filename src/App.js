import React, {Component} from 'react';
import { Route, Switch, Redirect } from "react-router-dom";
import {getLocalStore} from "instruments";

import Login from './components/Login'
import InstallPassword from './components/Login/InstallPassword'
import LoginPage from './components/Login/LoginPage'
import BDI from './components/Stats/BDI/Test'
import HDU_Stories from './components/Stats/HDU/History'
import BDI_Stories from './components/Stats/BDI/History'
import HistoryInformationBDI from './components/Stats/BDI/HistoryInformation'
import HistoryInformationHDU from './components/Stats/HDU/HistoryInformation'
import BDI_Stats from './components/Stats/BDI'
import HDU from './components/Stats/HDU'
import HDU_Test from './components/Stats/HDU/Test'
import Home from './components/Home'
import Sync from './components/LoginComponent'
import Registration from './components/LoginComponent/Registration'
import RecoverPassword from './components/LoginComponent/Registration/RecoverPassword'

import JournalErrors from './components/Diary/JournalErrors'
import AddLogicError from './components/Diary/AddLogicError'
import ViewLogicError from './components/Diary/ViewLogicError'

import Books from './components/Books'
import StoryList from './components/Books/StoryList'
import Story from './components/Books/stories'

import Stats from './components/Stats'
import Policy from './components/Policy'
import Settings from './components/Settings'
import Info from './UI/Info'

export default class Router extends Component {

    render() {
        return (
            <Switch>
                    <Route exact path="/" render={(props) => {
                        return (
                            (getLocalStore('pin') !== null && window.login !== true) ? (<Redirect to="/login/enter"/>) :
                                (<Home {...props} />)
                        )
                    }} />
                <Route exact path="/" component={Home} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/login/add" component={InstallPassword} />
                <Route exact path="/login/enter" component={LoginPage} />
                <Route exact path="/diary-list" component={JournalErrors} />
                <Route exact path="/diary-list/add" component={AddLogicError} />
                <Route path="/diary-list/edit:id" component={AddLogicError} />
                <Route path="/diary-list/view:id" component={ViewLogicError} />

                <Route exact path="/books" component={Books} />
                <Route path="/books/story-list" component={StoryList} />
                <Route path="/books/:id" component={Story} />

                <Route path="/stats" component={Stats} />
                <Route exact path="/bdi/test" component={BDI} />
                <Route exact path="/bdi/stories" component={BDI_Stories} />
                <Route path="/bdi/:id" component={HistoryInformationBDI} />
                <Route path="/bdi" component={BDI_Stats} />

                <Route exact path="/hdu/stories" component={HDU_Stories} />
                <Route exact path="/hdu/test" component={HDU_Test} />
                <Route path="/hdu/:id" component={HistoryInformationHDU} />
                <Route path="/hdu" component={HDU} />

                <Route path="/settings" component={Settings} />

                <Route path="/info" component={Info} />

                <Route path="/policy" component={Policy} />

                <Route path="/sync" component={Sync} />
                <Route path="/registration" component={Registration} />
                <Route path="/recover" component={RecoverPassword} />
            </Switch>
        )
    }
}