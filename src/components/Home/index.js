import React, {Component} from 'react';
import './style.sass';

import { connect } from "react-redux";
import { Link } from 'react-router-dom';
import BottomNav from 'UI/BottomNav';

import Page from 'UI/Page';
import Header from 'UI/Header';
import Card from 'UI/Card';

import { doDatabaseStuff, getDiary } from 'api'
import {getAllData} from "redux/actions/DiaryAction";
import NewVersion from "constants/info/NewVersion";
import {getLocalStore, setLocalStore} from "instruments";

class Home extends Component {

    componentDidMount() {
        doDatabaseStuff().then(res => {
            getDiary().then(data => {
                this.props.getAllData(data)
            });
        });

        if(getLocalStore('settings') === null) {
            setLocalStore('settings',{logicErrorShowFields: {
                    logicError: true,
                    solution: true,
                    event: true,
                    conclusion: true,
                    errorsSelected: true,
                    moodSelected: true,
                    moodRange: true
                }});
        }
    }

    render() {
        console.log(this)
        return (
            <div className='home'>
                <Header
                    h1='Главная'
                    back={false}
                    menu={true}
                    newVersion={<NewVersion />}
                    {...this.props}
                />
                <Page>
                    <Link id='diary-list' to="/diary-list">
                        <Card>
                            <div className="choose">
                                <div className="choose-text">
                                    <h3>Дневник автоматических мыслей</h3>
                                    <span>Основной инструмент для работы с искаженными мыслями</span>
                                </div>
                            </div>
                        </Card>
                    </Link>
                </Page>
                <BottomNav
                    history={this.props.history}
                    value='/'
                />
            </div>
        )
    }
}

const mapStateToProps = state => {
    const { DiaryReducer } = state;
    return DiaryReducer
};

export default connect(mapStateToProps, {getAllData})(Home);