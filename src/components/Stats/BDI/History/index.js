import React, {Component} from 'react';
import Page from 'UI/Page';
import Header from 'UI/Header';
import Card from 'UI/Card';
import {connect} from "react-redux";
import {BDI_RESULT} from "constants/BDI";
import Badge from 'UI/Badge';

import '../style.sass'
import {Link} from "react-router-dom";

class AllStory extends Component {
    render() {
        return(
            <>
                <Header
                    h1='История прохождения теста'
                    back={true}
                    menu={false}
                    {...this.props}
                />
                <Page>
                    {[...this.props.BDIAll].reverse().map((i) => {
                        let res = BDI_RESULT(i.score);

                        return (
                            <Link key={i.id} to={`/bdi/${i.id}`}>
                                <Card className='bdi-history-card'>
                                    <span className='bdi-history-card__date'>{i.date}</span>
                                    <Badge style={{top: 13, left: 13, background: res.badgeColor, marginRight: 10}}>{i.score}</Badge>
                                    <h3>{res.description}</h3>
                                </Card>
                            </Link>
                        )
                    })}
                </Page>
            </>
        )
    }
}

const mapStateToProps = state => {
    const { BDIReducer } = state;
    return BDIReducer
};

export default connect(mapStateToProps, {})(AllStory);