import React, {Component} from 'react';

import Page from 'UI/Page';
import Button from 'UI/Button';
import Stepper from 'UI/Stepper';

import {Link} from "react-router-dom";

import {BDIAdd} from "redux/actions/BDIAction";
import { DateTime } from 'luxon';

import '../style.sass';
import {connect} from "react-redux";
import {doDatabaseStuff} from "api";
import Header from 'UI/Header';
import {BDI, BDI_RESULT} from "constants/BDI";

class Book extends Component {

    state = {
        current: 0,
        score: []
    };

    componentDidMount() {
        doDatabaseStuff()
    }

    next = (id) => {
        const { score, current } = this.state;

        setTimeout(() => {
            this.setState({
                current: current + 1,
                score: typeof id === "number" ? [...score, id] : []
            })
        }, 300)

    };

    createResult = () => {
        const { score } = this.state;

        let result = score.reduce((sum, item) => (
            sum += item
        ), 0);

        let testRes = BDI_RESULT(result);

        return (
            <div className='round-wrap'>
                <h4>Ваш результат</h4>
                <div style={{background: testRes.badgeColor}} className='round-res'>
                    {result}
                </div>
                <h5>{testRes.description}</h5>
                {testRes.text}
            </div>
        )
    };

    finishTest = () => {
        this.props.BDIAdd(this.state.score, DateTime.local())
    };

    render() {
        const { current, score } = this.state;

        return (
            <>
                <Header
                    h1='BDI тест на уровень депрессии'
                    back={'/stats'}
                    {...this.props}
                />
                <Page>
                    <div className='test-wrapper'>
                        <div className='test card'>
                            {
                                (current !== BDI.length) &&
                                <div>
                                    <Stepper
                                        steps={BDI.length}
                                        activeStep={score.length}
                                    />
                                    {
                                        BDI[current].map((item, id) => (
                                            <Button key={id} onClick={() => this.next(id)}> {item} </Button>
                                        ))
                                    }
                                </div>
                            }

                            {
                                current === BDI.length &&
                                <div>
                                    {this.createResult()}
                                    <Link to='/bdi' onClick={this.finishTest}> <Button>Выйти</Button> </Link>
                                </div>
                            }
                        </div>
                    </div>
                </Page>
            </>
        )
    }
}

const mapStateToProps = state => {
    const { BDIReducer } = state;
    return BDIReducer
};

export default connect(mapStateToProps, {BDIAction: BDIAdd, BDIAdd})(Book);