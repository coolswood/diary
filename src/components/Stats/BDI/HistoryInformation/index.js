import React, {Component} from 'react';
import {connect} from "react-redux";
import Page from 'UI/Page';
import Header from 'UI/Header';
import Card from 'UI/Card';
import Badge from 'UI/Badge';

import {BDI, BDI_RESULT, RES_COLOR} from "constants/BDI";

class HistoryInformation extends Component {
    render() {
        const { BDIAll, match } = this.props;

        let history = BDIAll.find(i => i.id == match.params.id);

        return (
            <>
                <Header
                    h1='История прохождения теста'
                    back={true}
                    menu={false}
                    {...this.props}
                />
                <Page>
                    <Card>
                        <Badge style={{top: 10, right: 0, background: BDI_RESULT(history.score).badgeColor, marginRight: 10}}>{history.score}</Badge>
                        <h3>{history.date}</h3>
                        <ol className='list'>
                            {
                                BDI.map((i, id) => {
                                    return (
                                        <li key={id} style={{color: RES_COLOR[history.result[id]]}}>{i[history.result[id]]}</li>
                                    )
                                })
                            }
                        </ol>
                    </Card>
                </Page>
            </>
        )
    }
}

const mapStateToProps = state => {
    const { BDIReducer } = state;
    return BDIReducer
};

export default connect(mapStateToProps, {})(HistoryInformation);