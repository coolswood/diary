import React, {PureComponent} from 'react';

import Page from 'UI/Page';
import Header from 'UI/Header';
import Card from 'UI/Card';
import Label from 'UI/Label';
import Alert from 'UI/Alert';
import LineChart from 'UI/LineChart';
import {connect} from "react-redux";
import Button from 'UI/Button';
import {Link} from "react-router-dom";
import Badge from 'UI/Badge';
import {BDI_RESULT} from "constants/BDI";

import './style.sass'

class BDI_Stats extends PureComponent {

    state = {
        massScore: []
    };

    componentDidMount() {
        let massScore = this.props.BDIAll.reduce((acc, i) => {
            acc.push(i.score);
            return acc
        }, []);

        this.setState({
            massScore: massScore
        })
    }

    render() {
        const { massScore } = this.state;
        const { BDIAll } = this.props;

        return (
            <>
                <Header
                    h1='BDI тест на уровень депрессии'
                    back={'/stats'}
                    menu={false}
                    {...this.props}
                />
                <Page>
                    <Label>График</Label>
                    {
                        massScore.length >= 2 ?
                        <Card>
                            <LineChart
                                data={massScore}
                            />
                        </Card> : <Card>
                                <Alert
                                    type='graph'
                                />
                        </Card>
                    }

                    <Label>История</Label>

                    {BDIAll.length > 3 &&
                        <Label to='bdi/stories' style={{float: 'right'}}>Показать все &rarr;</Label>
                    }

                    {BDIAll.length !== 0 ? [...BDIAll].reverse().splice(0, 3).map((i) => {
                        let res = BDI_RESULT(i.score);

                        return (
                            <Link key={i.id} to={`bdi/${i.id}`}>
                                <Card className='bdi-history-card'>
                                    <span className='bdi-history-card__date'>{i.date}</span>
                                    <Badge style={{top: 13, left: 13, background: res.badgeColor, marginRight: 10}}>{i.score}</Badge>
                                    <h3>{res.description}</h3>
                                </Card>
                            </Link>
                        )
                    }) :
                    <Card>
                        Здесь будет история результатов теста
                    </Card>
                    }

                    <Label>Описание</Label>

                    <Card>
                        <div className='test-text'>
                            <p>
                                BDI - один из самых эффективных способов выявить наличие депрессии и ее стадию.
                                Мы рекомендуем регулярно проходить этот тест и ориентироваться на его результат в процессе лечения.
                            </p>
                            <p>Несмотря на простоту в обращении, BDI-тест надежен. У научившихся его применять есть
                                возможность диагностировать депрессии. Большое количество исследований за прошедшие
                                десятилетия показало, что BDI и подобные ему тесты высокоэффективны для определения
                                нарушений.</p>
                        </div>
                        <Link to='bdi/test' > <Button>Пройти тест</Button> </Link>
                    </Card>

                </Page>
            </>
        )
    }
}

const mapStateToProps = state => {
    const { BDIReducer } = state;
    return BDIReducer
};

export default connect(mapStateToProps, {})(BDI_Stats);