import React, {Component} from 'react';
import './style.sass';

import { Link } from 'react-router-dom';
import BottomNav from 'UI/BottomNav';

import Page from 'UI/Page';
import Header from 'UI/Header';
import Card from 'UI/Card';
import Badge from 'UI/Badge';
import ProxyIcons from 'UI/ProxyIcons';
import {connect} from "react-redux";
import {doDatabaseStuff, getBDIData, getHDUData} from "api";
import {BDIStartData} from "redux/actions/BDIAction";
import {HDUStartData} from "redux/actions/HDUAction";

import StatsError from 'constants/info/StatsError'
import {BDI_RESULT} from "constants/BDI";
import {HDUimgs} from "constants/HDU";

class Stats extends Component {

    componentDidMount() {
        doDatabaseStuff().then(res => {
            getBDIData().then(data => {
                this.props.BDIStartData(data)
            });
            getHDUData().then(data => {
                this.props.HDUStartData(data)
            })
        });
    }

    render() {
        const { BDIAll } = this.props.BDIReducer;
        const { HDUAll } = this.props.HDUReducer;
        let lastTest = BDIAll[BDIAll.length - 1];
        let lastHDU = HDUAll[HDUAll.length - 1];

        let bdiRes = {};

        if(!!lastTest) {
            bdiRes = BDI_RESULT(lastTest.score);
        }

        return (
            <div className='home'>
                <Header
                    h1='Статистика'
                    back={false}
                    menu={true}
                    info={<StatsError />}
                    {...this.props}
                />
                <Page>
                    <Link to="/bdi">
                        <Card className='bdi'>
                            {!!lastTest && <Badge style={{bottom: 10, right: 10, background: bdiRes.badgeColor}}>{lastTest.score}</Badge>}
                            <div className="choose">
                                <div className="choose-text">
                                    <h3>BDI тест на уровень депрессии</h3>
                                    <span>Один из основополагающих тестов на уровень депресии.</span>
                                    <span><b>Рекомендация по прохождению:</b> 1 раз в неделю</span>
                                    <span><b>Пройден: </b>{BDIAll.length === 0 ? '-' : lastTest.date}</span>
                                </div>
                            </div>
                        </Card>
                    </Link>
                    <Link to="/hdu">
                        <Card>
                            <div className="choose">
                                <div className="choose-text">
                                    <h3>Шкала дисфункциональных убеждений</h3>
                                    <span>Определите в каких сферах жизни у вас присутствуют дисфункциональные убеждения.</span>
                                    <span><b>Рекомендация по прохождению:</b> 1 раз в месяц</span>
                                    <span><b>Пройден: </b>{HDUAll.length === 0 ? '-' : lastHDU.date}</span>
                                    <div className="choose-imgs">
                                        {HDUAll.length !== 0 &&
                                            HDUAll[HDUAll.length - 1].result.map((i, id) => {
                                                if(i < 0) {
                                                    return <ProxyIcons
                                                        src={HDUimgs[id]}
                                                        className="hdu-img"
                                                    />
                                                }
                                                return true;
                                            })
                                        }
                                    </div>
                                </div>
                            </div>
                        </Card>
                    </Link>
                </Page>
                <BottomNav
                    history={this.props.history}
                    value='/stats'
                />
            </div>
        )
    }
}

const mapStateToProps = state => {
    const { BDIReducer, HDUReducer } = state;
    return { BDIReducer, HDUReducer }
};

export default connect(mapStateToProps, {BDIStartData, HDUStartData})(Stats);