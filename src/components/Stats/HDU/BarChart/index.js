import React, {Component} from 'react';

import ReactEchartsCore from 'echarts-for-react/lib/core';
import echarts from 'echarts/lib/echarts';
import 'echarts/lib/chart/bar';
import 'echarts/lib/component/visualMap';

import {COLOR_EXTRA, COLOR_EXTRIM, COLOR_HARD, COLOR_LIGHT, COLOR_MIDDLE, COLOR_NORMAL} from "constants/index";
import {HDUgroups} from "constants/HDU";

export default class RadarChart extends Component {
    state = {
        options: {}
    };

    componentDidMount() {
        this.setState({
            options: this.getOption()
        })
    }

    getOption = () => {
        const { data } = this.props;

        return {
            grid: {
              top: 20,
              left: 20,
              right: 0,
              bottom: 80
            },
            xAxis : [
                {
                    type : 'category',
                    data : HDUgroups,
                    axisLabel: {
                        interval: 0,
                        rotate: 60,
                        fontSize: 10
                    },
                    axisTick: {
                        show: false
                    },
                    axisLine: {
                        show: false
                    }
                }
            ],
            yAxis : [
                {
                    type : 'value',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    max: 10,
                    min: -10,
                }
            ],
            visualMap: {
                show: false,
                pieces: [
                {
                    gt: -11,
                    lte: -6,
                    color: COLOR_EXTRA
                },
                {
                    gt: -6,
                    lte: -4,
                    color: COLOR_EXTRIM
                },
                {
                    gt: -4,
                    lte: 0,
                    color: COLOR_HARD
                },
                {
                    gt: 0,
                    lte: 4,
                    color: COLOR_MIDDLE
                },
                {
                    gt: 4,
                    lte: 6,
                    color: COLOR_LIGHT
                },
                {
                    gt: 6,
                    lte: 11,
                    color: COLOR_NORMAL
                }, ]
            },
            series : [
                {
                    type:'bar',
                    barWidth: '60%',
                    data: data
                }
            ]
        }
    };

    render() {
        return (
            <ReactEchartsCore
                echarts={echarts}
                option={this.state.options}
                style={{height: '250px'}}
                notMerge={true}
            />
        )
    }
}