import React, {Component} from 'react';
import {connect} from "react-redux";
import Page from 'UI/Page';
import Header from 'UI/Header';
import Card from 'UI/Card';
import ProxyIcons from 'UI/ProxyIcons';

import RadarChart from '../BarChart'
import Label from 'UI/Label';
import {hdu, HDU_RES_COLOR, HDUgroups, HDUimgs} from "constants/HDU";

class HistoryInformation extends Component {

    constructor(props) {
        super(props)

        this.cnt = 0;
    }

    render() {
        const { HDUAll, match } = this.props;

        let history = HDUAll.find(i => i.id == match.params.id);

        return (
            <>
                <Header
                    h1='История прохождения теста'
                    back={true}
                    menu={false}
                    {...this.props}
                />
                <Page>
                    {history && <>
                        <Label>График</Label>
                        <Card>
                            <RadarChart
                                data={history.result}
                            />
                        </Card>
                    </>}
                    <Label>Результат</Label>
                    <Card>
                        <ol className='list'>
                            {
                                history.steps.map((i, id) => {
                                    return (
                                        <>
                                        {id % 5 === 0 &&
                                            <div style={{display: 'flex', alignItems: 'center', margin: '20px 0'}}>
                                                <ProxyIcons
                                                    src={HDUimgs[this.cnt]}
                                                    className="hdu-img"
                                                    style={{marginRight: 10}}
                                                />
                                                <span>{HDUgroups[this.cnt++]}</span>
                                            </div>
                                        }
                                        <li key={id} style={{color: HDU_RES_COLOR[history.steps[id] + 2]}}>{hdu[id]}</li>
                                        </>
                                    )
                                })
                            }
                        </ol>
                    </Card>
                </Page>
            </>
        )
    }
}

const mapStateToProps = state => {
    const { HDUReducer } = state;
    return HDUReducer
};

export default connect(mapStateToProps, {})(HistoryInformation);