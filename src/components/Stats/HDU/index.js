import React, {PureComponent} from 'react';

import Page from 'UI/Page';
import Header from 'UI/Header';
import Card from 'UI/Card';
import Label from 'UI/Label';
import {connect} from "react-redux";
import Button from 'UI/Button';
import {Link} from "react-router-dom";
import ProxyIcons from 'UI/ProxyIcons';

import './style.sass'
import {HDUgroups, HDUimgs} from "constants/HDU";

class HDU_Stats extends PureComponent {

    state = {
        massScore: []
    };

    render() {
        const { HDUAll } = this.props;

        return (
            <>
                <Header
                    h1='BDI тест на уровень депрессии'
                    back={'/stats'}
                    menu={false}
                    {...this.props}
                />
                <Page>
                    <Label>История</Label>

                    {HDUAll.length > 3 &&
                        <Label to='hdu/stories' style={{float: 'right'}}>Показать все &rarr;</Label>
                    }
                    {
                        HDUAll.length !== 0 ? [...HDUAll].reverse().splice(0, 3).map((i) => {
                            return (
                                <Link key={i.id} to={`hdu/${i.id}`}>
                                    <Card className='hdu-history-card'>
                                        <h3 className='hdu-history-card__date'>{i.date}</h3>
                                        {i.result.map((item, id) => (
                                            <span style={{display: 'flex', alignItems: 'center', margin: '10px 0'}} key={HDUimgs[id]}>
                                                <ProxyIcons
                                                    src={HDUimgs[id]}
                                                    className="hdu-img"
                                                    style={{marginRight: 10}}
                                                />
                                                {HDUgroups[id]}: {item}
                                            </span>
                                        ))}
                                    </Card>
                                </Link>
                            )
                        }) :
                        <Card>
                            Здесь будет история результатов теста
                        </Card>
                    }
                    <Label>Описание</Label>
                    <Card>
                        <div className='test-text'>
                            <h2>Шкала дисфункциональных убеждений</h2>
                            <span>
                                            Выберите утверждение, которое точнее
                                            всего отражает вашу оценку собственных мыслей бóльшую часть времени. В каждом из
                                            случаев выберите только один ответ. Поскольку мы все разные, никаких «правильных»
                                            или «неправильных» ответов здесь нет. Чтобы оценить, типично ли то или иное
                                            отношение для вашей философии, вспомните, как вы смотрите на вещи бóльшую часть
                                            времени.
                                        </span>
                            <span>Каждая группа из пяти утверждений данной шкалы измеряет одну из семи систем убеждений. Общий балл для каждой группы утверждений может варьироваться от +10 до –10.</span>
                            <span>Чем выше ваш былл в каждом из убеждений, тем более здоровы вы в данной области.</span>
                        </div>
                        <Link to='hdu/test' > <Button>Начать</Button> </Link>
                    </Card>
                </Page>
            </>
        )
    }
}

const mapStateToProps = state => {
    const { HDUReducer } = state;
    return HDUReducer
};

export default connect(mapStateToProps, {})(HDU_Stats);