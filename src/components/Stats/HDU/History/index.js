import React, {Component} from 'react';
import Page from 'UI/Page';
import Header from 'UI/Header';
import Card from 'UI/Card';
import {connect} from "react-redux";

import '../style.sass'
import {Link} from "react-router-dom";
import {HDUgroups, HDUimgs} from "constants/HDU";
import ProxyIcons from 'UI/ProxyIcons';

class AllStory extends Component {
    render() {
        return(
            <>
                <Header
                    h1='История прохождения теста'
                    back={true}
                    menu={false}
                    {...this.props}
                />
                <Page>
                    {[...this.props.HDUAll].reverse().map((i) => {

                        return (
                            <Link key={i.id} to={`/hdu/${i.id}`}>
                                <Card>
                                    <h3 className='hdu-history-card__date'>{i.date}</h3>
                                    {i.result.map((item, id) => (
                                        <span style={{display: 'flex', alignItems: 'center', margin: '10px 0'}} key={HDUimgs[id]}>
                                                <ProxyIcons
                                                    src={HDUimgs[id]}
                                                    className="hdu-img"
                                                    style={{marginRight: 10}}
                                                />
                                            {HDUgroups[id]}: {item}
                                            </span>
                                    ))}
                                </Card>
                            </Link>
                        )
                    })}
                </Page>
            </>
        )
    }
}

const mapStateToProps = state => {
    const { HDUReducer } = state;
    return HDUReducer
};

export default connect(mapStateToProps, {})(AllStory);