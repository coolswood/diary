import React, {Component} from 'react';

import Page from 'UI/Page';
import Button from 'UI/Button';
import Stepper from 'UI/Stepper';
import Card from 'UI/Card';
import Label from 'UI/Label';
import HDUErrorsList from 'UI/HDUErrorsList'
import ProxyIcons from 'UI/ProxyIcons';

import {Link} from "react-router-dom";

import {HDUAdd} from "redux/actions/HDUAction";
import { DateTime } from 'luxon';

import '../style.sass';
import {connect} from "react-redux";
import {HDUgroups, buttons, hdu, HDUimgs} from "constants/HDU";
import Header from 'UI/Header';
import RadarChart from '../BarChart'

let cnt = [];

class HDU extends Component {

    state = {
        current: 0,
        score: [],
        steps: []
    };

    next = (id) => {
        const { score, current, steps } = this.state;

        if(typeof id === "number") {
            cnt.push(id)
        }

        if(cnt.length === 5) {
            let res = cnt.reduce((sum, item) => (
                sum += item
            ));

            this.setState({
                score: [...score, res]
            });

            cnt = [];
        }

        this.setState({
            steps: typeof id === "number" ? [...steps, id] : [],
            current: current + 1
        })
    };

    createResult = () => {
        const { score } = this.state;

        return (
            <>
                <Label>Ваш результат</Label>
                <Card>
                    {score.map((item, id) => (
                        <span style={{display: 'flex', alignItems: 'center', margin: '10px 0'}} key={HDUimgs[id]}>
                            <ProxyIcons
                                src={HDUimgs[id]}
                                className="hdu-img"
                                style={{marginRight: 10}}
                            />
                            {HDUgroups[id]}: {item}
                    </span>
                    ))}
                </Card>
                <Label>Описание</Label>
                <div style={{marginBottom: 20}}>
                    <HDUErrorsList />
                </div>
                <Label>График</Label>
                <Card>
                    <RadarChart
                        data={score}
                    />
                </Card>
                <Label>Информация</Label>
                <Card>
                    <span>Положительный балл отражает, что в данной области вы психологически сильны, это ваша зона уверенности. Отрицательный балл говорит о том, что в данной области вы эмоционально уязвимы. </span>
                    <Link to='/stats' onClick={this.finishTest}> <Button>Выйти</Button> </Link>
                </Card>
            </>
        )
    };

    finishTest = () => {
        const { score, steps } = this.state;

        this.props.HDUAdd(score, steps, DateTime.local())
    };

    render() {
        const { current, steps } = this.state;

        return (
            <>
                <Header
                    h1='Шкала дисфункциональных убеждений'
                    back={current !== hdu.length ? '/stats' : false}
                    {...this.props}
                />
                <Page>
                    <div className='test-wrapper'>
                        <div className='test'>
                            {
                                (current !== -1 && current !== hdu.length) &&
                                <Card>
                                    <Stepper
                                        steps={hdu.length}
                                        activeStep={steps.length}
                                    />
                                    <h3>{hdu[current]}</h3>
                                    {buttons.map((item, id) => (
                                        <Button key={id} onClick={() => this.next(item.score)}> {item.name} </Button>
                                    ))}
                                </Card>
                            }

                            {
                                current === hdu.length &&
                                <div>
                                    {this.createResult()}
                                </div>
                            }
                        </div>
                    </div>
                </Page>
            </>
        );
    }
}

const mapStateToProps = state => {
    const { HDUReducer } = state;
    return HDUReducer
};

export default connect(mapStateToProps, {HDUAdd})(HDU);