import React, {useState, useEffect} from 'react';
import cn from 'classnames';
import isEqual from 'lodash/isEqual';
import {CSSTransition} from 'react-transition-group'
import {setLocalStore} from "instruments";

import Header from 'UI/Header';
import Page from 'UI/Page';

import '../style.sass';

export default (props) => {
    const [itemCount, addItem] = useState([]);
    const [itemCountReturn, addItemReturn] = useState([]);
    const [status, setStatus] = useState(null);

    const addItemCount = (e) => {
        if (itemCount.length !== 4) {
            addItem([...itemCount, +e.target.innerText])
        } else {
            addItemReturn([...itemCountReturn, +e.target.innerText]);
        }
    };

    const removeItemCount = () => {
        if (itemCount.length < 4) {
            let newItems = [...itemCount];
            newItems.splice(newItems.length - 1, 1);
            return addItem(newItems)
        }

        if (itemCountReturn.length < 4) {
            let newItems = [...itemCountReturn];
            newItems.splice(newItems.length - 1, 1);
            return addItemReturn(newItems)
        }
    };

    useEffect(() => {
        if (itemCountReturn.length === 4) {
            if (isEqual(itemCount, itemCountReturn)) {
                setLocalStore('pin', itemCount);
                setStatus('success');
                window.login = true;

                setTimeout(() => {
                    props.history.push('/');
                }, 800)
            } else {
                setStatus('wrong');

                setTimeout(() => {
                    setStatus(null);
                    addItemReturn([])
                }, 800)
            }
        }
    }, [itemCountReturn])

    return (
        <div>
            <Header
                h1='Пароль'
                back={true}
                {...props}
            />
            <Page>
                <div id="pincode">
                    <div className="table">
                        <div className="cell">

                            <h3>Введите код доступа</h3>

                            <div className="fields">
                                <div className="grid">
                                    {Array.from({length: 4}).map((i, id) => (
                                        <div
                                            className={cn("grid__col grid__col--1-of-4 numberfield", {"active": id + 1 <= itemCount.length})}>
                                            <span></span></div>
                                    ))}
                                </div>
                            </div>

                            <CSSTransition
                                in={itemCount.length === 4}
                                classNames="login"
                                timeout={700}
                                unmountOnExit
                            >
                                <div>
                                    <h3>Введите код доступа</h3>

                                    <div className="fields">
                                        <div className="grid">
                                            {Array.from({length: 4}).map((i, id) => (
                                                <div className={cn("grid__col grid__col--1-of-4 numberfield",
                                                    {"active": id + 1 <= itemCountReturn.length},
                                                    {[status]: status !== null}
                                                )}><span></span></div>
                                            ))}
                                        </div>
                                    </div>
                                </div>
                            </CSSTransition>

                            <div id="numbers">
                                <div className="grid">
                                    {
                                        Array.from({length: 9}).map((i, id) => (
                                            <div className="grid__col grid__col--1-of-3">
                                                <button onClick={addItemCount}>{id + 1}</button>
                                            </div>
                                        ))
                                    }

                                    <div className="grid__col grid__col--1-of-3"/>
                                    <div className="grid__col grid__col--1-of-3">
                                        <button onClick={addItemCount}>0</button>
                                    </div>
                                    <div className="grid__col grid__col--1-of-3">
                                        {((!!itemCount.length && itemCount.length < 4) || (!!itemCountReturn.length && itemCountReturn.length < 4)) &&
                                        <div data-test="delete" style={{fontSize: 40}} className="pincode__actions"
                                             onClick={removeItemCount}>
                                            &#8656;
                                        </div>}
                                    </div>
                                    <div className="grid__col grid__col--1-of-3"/>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </Page>
        </div>
    )
}