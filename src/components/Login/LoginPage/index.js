import React, { useState, useEffect } from 'react';
import cn from 'classnames';
import isEqual from 'lodash/isEqual';
import {getLocalStore} from "instruments";

import Page from 'UI/Page';

import '../style.sass';
import RestirePin from "../RestirePin";

export default (props) => {
    const [itemCount, addItem] = useState([]);
    const [status, setStatus] = useState(null);
    const [isReset, setReset] = useState(false);

    const addItemCount = (e) => {
        if(itemCount.length !== 4) {
            addItem([...itemCount, +e.target.innerText])
        }
    };

    const removeItemCount = () => {
        if(itemCount.length < 4) {
            let newItems = [...itemCount];
            newItems.splice(newItems.length - 1, 1);
            return addItem(newItems)
        }
    };

    const restore = () => {
        setReset(true)
    };

    useEffect(() => {
        if(itemCount.length === 4) {
            if(isEqual(itemCount, getLocalStore('pin'))) {
                setStatus('success');
                window.login = true;

                setTimeout(() => {
                    props.history.push('/');
                }, 800)
            } else {
                setStatus('wrong');

                setTimeout(() => {
                    setStatus(null);
                    addItem([]);
                }, 800)
            }
        }
    }, [itemCount]);

    if(isReset) {
        return <RestirePin {...props} />
    }

    return (
        <Page>
            <div id="pincode">
                <div className="table">
                    <div className="cell">

                        <h3>Введите код доступа</h3>

                        <div className="fields">
                            <div className="grid">
                                {Array.from({ length: 4 }).map((i, id) => (
                                    <div className={cn("grid__col grid__col--1-of-4 numberfield", {"active": id + 1 <= itemCount.length}, {[status]: status !== null})}><span></span></div>
                                ))}
                            </div>
                        </div>

                        <div id="numbers">
                            <div className="grid">
                                {
                                    Array.from({ length: 9 }).map((i, id) => (
                                        <div className="grid__col grid__col--1-of-3">
                                            <button onClick={addItemCount}>{id + 1}</button>
                                        </div>
                                    ))
                                }

                                <a className="grid__col grid__col--1-of-3">
                                    <div className="pincode__actions reset" onClick={restore}>Сброс</div>
                                </a>
                                <div className="grid__col grid__col--1-of-3">
                                    <button onClick={addItemCount}>0</button>
                                </div>
                                <div className="grid__col grid__col--1-of-3">
                                    {(!!itemCount.length && itemCount.length < 4) && <div data-test="delete" style={{fontSize: 40}} className="pincode__actions" onClick={removeItemCount}><span className='visualy-hidden'>Удалить</span>&#8656;</div>}
                                </div>
                                <div className="grid__col grid__col--1-of-3" />

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </Page>
    )
}