import React, {useState} from 'react';

import Page from 'UI/Page';
import Header from 'UI/Header';
import Card from 'UI/Card';
import Button from 'UI/Button';
import {Link} from "react-router-dom";

import {getLocalStore, setLocalStore} from "instruments";

export default (props) => {
    const [pin, setPin] = useState(getLocalStore('pin'));

    const reset = () => {
        setLocalStore('pin', null);
        setPin(null)
    };

    return (
        <>
            <Header
                h1='Пароль'
                back={true}
                {...props}
            />
            <Page>
                <Card>
                    <p>В данном разделе вы можете установить код доступа для защиты ваших данных. Данный код будет запрашиваться при каждом открытии приложения.</p>
                    <p><b>Внимание! </b>В случае, если вы забыли свой код доступа при входе, сбросить его вы можете только вместе с удалением всех ваших записей. При этом
                    данные останутся на нашем сервере и вы сможете их восстановить (в случае если вы настроили синхронизацию).
                    </p>
                    <Button disabled={pin}><Link to="/login/add">Установить пароль</Link></Button>
                    <Button disabled={!pin} color="secondary" onClick={reset}>Сбросить пароль</Button>
                </Card>
            </Page>
        </>
    )
}