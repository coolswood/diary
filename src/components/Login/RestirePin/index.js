import React from 'react';
import Page from 'UI/Page';
import Card from 'UI/Card';
import Button from 'UI/Button';
import {Link} from "react-router-dom";

export default (props) => {

    const reset = () => {
        localStorage.clear();
        indexedDB.deleteDatabase('PSY');
        props.history.push('/')
    };

    return (
        <Page>
            <Card style={{marginTop: 30}}>
                <p><b>Внимание! </b>Сбросить пароль входа можно только с удалением всех данных из приложения. При этом данные, сохраненные на сервере останутся.</p>
                <Button onClick={reset} color="secondary">Сбросить</Button>
                <Link to="/"><Button>Вернуться</Button></Link>
            </Card>
        </Page>
    )
}