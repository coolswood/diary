import React, {Component} from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Page from 'UI/Page';
import Header from 'UI/Header';
import Card from 'UI/Card';

import './style.sass';
import {getLocalStore, setLocalStore} from "instruments";

const SETTNGS_LIST = [
    {
        h: 'Отображение полей на странице записи мыслей',
        labels: [
            {
                id: 'event',
                text: 'Событие / ситуация / триггер'
            },
            // {
            //     id: 'logicError',
            //     text: 'Автоматические мысли'
            // },
            {
                id: 'errorsSelected',
                text: 'Выберите логическую ошибку'
            },
            {
                id: 'moodSelected',
                text: 'Выберите вашу эмоцию'
            },
            {
                id: 'moodRange',
                text: 'Уровень эмоционального расстройства'
            },
            // {
            //     id: 'solution',
            //     text: 'Адекватная оценка'
            // },
            {
                id: 'conclusion',
                text: 'Вывод'
            },
        ]
    }
];

export default class Settings extends Component {

    constructor(props) {
        super(props);

        this.state = {
            ...getLocalStore('settings').logicErrorShowFields
        }
    }

    toggleSelector = (type) => {
        this.setState({
            [type]: !this.state[type]
        })
    };

    render() {

        setLocalStore('settings',{logicErrorShowFields: {
                ...this.state
            }});
        
        return(
            <div className='settings'>
                <Header
                    h1='Статистика'
                    back={true}
                    {...this.props}
                />
                <Page>
                    <Card>
                        {SETTNGS_LIST.map(i => {
                            return (
                                <div key={i.h}>
                                <h3>{i.h}</h3>
                                <div className="settings__labels">
                                    {i.labels.map(({id, text}) => (
                                        <FormControlLabel
                                            control={
                                                <Switch
                                                    checked={this.state[id]}
                                                    onChange={() => this.toggleSelector(id)}
                                                    value="logicError"
                                                    color="primary"
                                                />
                                            }
                                            key={id}
                                            labelPlacement="start"
                                            className='settings__item'
                                            label={text}
                                        />
                                    ))}
                                </div>
                                </div>
                            )
                        })}
                    </Card>
                </Page>
            </div>
        )
    }
}