import React, {useEffect, useState} from "react";
import Header from 'UI/Header';
import Page from 'UI/Page';
import Card from 'UI/Card';
import Button from 'UI/Button';
import {getLocalStore, setLocalStore, ApiPost} from "instruments";
import {getAllData, restoreAllData} from 'api'
import {DateTime} from "luxon";

import {Link} from "react-router-dom";

export default (props) => {

    const [authorization, setAuthorization] = useState(null);
    const [lastBackup, setLastBackup] = useState('');

    useEffect(() => {
        let authorization = getLocalStore('authorization');
        setAuthorization(authorization);

        if(authorization) {
            ApiPost('/api/lastUpdate', {login: authorization.login}).then(res => {
                setLastBackup(res.lastUpdate);
            })
        }

    }, []);

    const backup = () => {
        getAllData().then(r => {
            const currentTime = DateTime.local().toLocaleString(DateTime.DATETIME_MED_WITH_SECONDS);

            ApiPost('/api/backup', {...authorization, data: r, lastUpdate: currentTime}).then(res => {
                if(res.status === 'OK') {
                    setLastBackup(currentTime);
                }
            })
        })
    };

    const restore = () => {
        ApiPost('/api/restore', {login: authorization.login}).then(res => {
            restoreAllData(res.data.data).then(r => {

            })
        })
    };

    const out = () => {
        setLocalStore('authorization', null);
        setAuthorization(null);
    };

    return (
        <div className='edit-mind'>
            <Header
                h1='Синхронизация данных'
                {...props}
            />
            <Page>
                {!!authorization ?
                    <Card>
                        <p>Вы можете загрузить данные в хранилище на сервере, для этого нажмите кнопку <b>сохранить на сервер</b>.</p>
                        <p>Вы можете восстановить данные на вашем устройстве, для этого нажмите кнопку <b>загрузить с сервера</b>.</p>
                        <p><b>Внимание!</b> Каждая операция приводит данные на сервере и устройстве к одному виду. Например, если вы внесли
                        записи на устройстве, которые не синхронизировали с сервером, а потом нажали кнопку <b>загрузить с сервера</b> несинхронизированные
                        данные с устройства будут удалены.
                        </p>
                        <Button onClick={backup}>Сохранить на сервер</Button>
                        <Button disabled={!lastBackup} onClick={restore}>Загрузить с сервера</Button>
                        {lastBackup ?
                        <div>
                            <div>Дата последней загрузки на сервер:</div>
                            <div>{lastBackup}</div>
                        </div> : <div>Нет сохранений на сервере</div>}
                        <br/>
                        <Button color="secondary" onClick={out}>Выйти</Button>
                    </Card> :
                    <Card>
                        <div>Для того, чтобы не потерять текущие записи, а также прогресс в тестах, вы можете сохранить данные на нашем сервере.
                        При этом мы гарантируем кофиденциальность - все записи хранятся в зашифрованном виде. Из личных данных нам требуется только
                        Email для возможности восстановить забытый пароль.
                        </div>
                        <br/>
                        <Button><Link to="/registration">Зарегистрироваться / Войти</Link></Button>
                    </Card>
                }
            </Page>
        </div>
    )
}