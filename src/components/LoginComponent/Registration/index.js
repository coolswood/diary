import React, {useEffect, useState} from "react";
import {ApiPost} from "instruments"
import Header from 'UI/Header';
import TextField from 'UI/TextField';
import Page from 'UI/Page';
import Button from 'UI/Button';
import {setLocalStore} from "instruments";

import { Link } from 'react-router-dom';

import isEmail from 'validator/lib/isEmail';

export default (props) => {

    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    const [errorLogin, setErrorLogin] = useState('');
    const [errorPassword, setErrorPassword] = useState('');

    const validateData = (e) => {
        e.preventDefault();

        setErrorLogin('');
        setErrorPassword('');

        if(login === '') {
            return setErrorLogin('Это обязательное поле')
        }

        if(!isEmail(login)) {
            return setErrorLogin('Заполните Email правильно')
        }

        if(password === '') {
            return setErrorPassword('Это обязательное поле')
        }

        if(password.length < 6 || password.length > 12) {
            return setErrorPassword('Пароль должен быть не менее 6 и не более 12 символов')
        }

        sendData()
    };

    const sendData = () => {
        ApiPost('/api/register', {
            login: login,
            password: password
        }).then(res => {

            if(res.status === "wrongPassword") {
                return setErrorPassword(res.text)
            }

            setLocalStore('authorization', {
                login: login,
                password: password
            });

            props.history.goBack();

        }).catch(error => {
            return setErrorLogin(error.text)
        })
    };

    return (
        <div>
            <Header
                h1='Регистрация / Вход'
                {...props}
            />
            <Page>
                <form onSubmit={validateData}>
                    <TextField
                        className='field-event'
                        tipeText={(type, text) => setLogin(text)}
                        value={login}
                        error={errorLogin}
                        errorMessage={errorLogin}
                        type='login'
                        label="Email"
                    />
                    <TextField
                        className='field-event'
                        tipeText={(type, text) => setPassword(text)}
                        error={errorPassword}
                        errorMessage={errorPassword}
                        value={password}
                        type='password'
                        label="Пароль"
                    />
                    <Button>Зарегистрироваться / Войти</Button>
                    <Link to="/recover"><Button color="default" type="button">Не помню пароль</Button></Link>
                </form>
            </Page>
        </div>
    )
}