import React, {useState} from "react";
import {ApiPost} from "instruments"
import Header from 'UI/Header';
import TextField from 'UI/TextField';
import Page from 'UI/Page';
import Button from 'UI/Button';
import Message from 'UI/Message';

import isEmail from 'validator/lib/isEmail';

export default (props) => {

    const [login, setLogin] = useState('');
    const [errorLogin, setErrorLogin] = useState('');
    const [successLogin, setSuccessLogin] = useState('');

    const recoverPassword = (e) => {
        e.preventDefault();

        ApiPost('/api/getPassword', {login: login}).then(res => {

            if(res.status === "wrongLogin") {
                return setErrorLogin(res.text);
            }

            setSuccessLogin(res.text);
            setLogin('');
        })
    };

    const validateData = (e) => {
        e.preventDefault();

        setErrorLogin('');
        setSuccessLogin('');

        if(login === '') {
            return setErrorLogin('Это обязательное поле')
        }

        if(!isEmail(login)) {
            return setErrorLogin('Заполните Email правильно')
        }

        recoverPassword(e)
    };

    return (
        <div>
            <Header
                h1='Восстановить пароль'
                {...props}
            />
            <Page>
                <form onSubmit={validateData}>
                    <TextField
                        className='field-event'
                        tipeText={(type, text) => setLogin(text)}
                        value={login}
                        error={errorLogin}
                        errorMessage={errorLogin}
                        type='login'
                        label="Email"
                    />
                    <Button>Выслать пароль на Email</Button>
                    {successLogin && <Message type="success">{successLogin}</Message>}
                </form>
            </Page>
        </div>
    )
}