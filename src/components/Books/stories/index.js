import React from 'react';

import Page from 'UI/Page';
import Header from 'UI/Header';
import Card from 'UI/Card';
import ChatBubble from 'UI/ChatBubble'
import ProxyIcons from 'UI/ProxyIcons';

import {stories} from './text'
import {getLocalStore, setLocalStore} from "instruments";

export default (props) => {
    const story = stories.find((i) => {
       return i.id === props.match.params.id
    });

    const readStories = getLocalStore('readStories');

    const readStoriesSet = new Set(readStories);

    readStoriesSet.add(props.match.params.id);

    setLocalStore('readStories', Array.from(readStoriesSet));

    return (
        <>
            <Header
                h1='История'
                back={true}
                {...props}
            />
            <Page>
                <Card>
                    {!!story.logicErrors.length && <div className="logic-errors">
                        {story.logicErrors.map(item => (
                            <ProxyIcons
                                src={item}
                                className='logic-error-item'
                            />
                        ))}
                    </div>}
                    <h3 style={{paddingBottom: 10}}>{story.header}</h3>
                    {story.title}
                    {story.chat.map((i, id) => (
                        <ChatBubble
                            key={id}
                            chat={[
                                {
                                    type: i.type,
                                    messages: i.messages
                                }
                            ]}
                        />
                    ))}
                    {story.text}
                    <div className='copyright'>{story.copyright}</div>
                </Card>
            </Page>
        </>
    )
}