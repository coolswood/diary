import React from 'react';
import Page from 'UI/Page';
import Header from 'UI/Header';

import {stories} from '../stories/text'
import {getLocalStore} from "instruments";
import {renderStoryItem, sortStory} from "components/Books/helper";

export default (props) => {
    const readStories = getLocalStore('readStories');
    let renderStores = sortStory(stories, readStories);

    return (<>
        <Header
            h1='Истории'
            back={true}
            {...props}
        />
        <Page>
            {renderStores.map((i) => {
                let isRead = readStories.includes(i.id);

                return renderStoryItem(i, isRead)
            })}
        </Page>
    </>)
}