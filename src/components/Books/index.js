import React, {Component} from 'react';
import './style.sass';

import BottomNav from 'UI/BottomNav';

import Page from 'UI/Page';
import Header from 'UI/Header';
import Card from 'UI/Card';

import Label from 'UI/Label';

import {stories} from './stories/text'
import {Link} from "react-router-dom";
import {sortStory, renderStoryItem} from "components/Books/helper";

export default class Books extends Component {

    render() {

        let readStories = localStorage.getItem('readStories');

        if(readStories === null) {
            localStorage.setItem('readStories', '[]');

            readStories = [];
        }

        let renderStory = sortStory(stories, readStories)[0];

        let isRead = readStories.includes(renderStory.id);

        return (
            <div className='home'>
                <Header
                    h1='Чтение'
                    back={false}
                    menu={true}
                    {...this.props}
                />
                <Page>
                    <Label>Книги</Label>
                    <a href="https://www.litres.ru/pages/quickread/?art=40975505&skin=normal&lfrom=248829566&widget=1.00&iframe=1">
                        <Card className='berns-book'>
                            <div className="choose">
                                <div className="choose-text">
                                    <h3>Терапия настроения. Клинически доказанный способ победить депрессию без таблеток</h3>
                                    <span>Автор: Бернс Дэвид</span>
                                    <span></span>
                                </div>
                            </div>
                        </Card>
                    </a>
                    <Label>Истории</Label>

                    <Label
                        style={{float: 'right'}}
                        to='books/story-list'
                    >Все истории &rarr;</Label>

                    {renderStoryItem(renderStory, isRead)}

                </Page>
                <BottomNav
                    history={this.props.history}
                    value='/books'
                />
            </div>
        )
    }
}