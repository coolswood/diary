import {sortStory} from "components/Books/helper";

describe('sortStory', () => {
    const enterData = [
        {id:"1"},
        {id:"2"},
        {id:"3"},
        {id:"4"},
    ];

    it('Нет прочитанных историй', () => {
        expect(sortStory(enterData, [])).toEqual(enterData)
    });

    it('Две прочитанные истории', () => {
        expect(sortStory(enterData, ["1", "2"])).toEqual([
            {id:"3"},
            {id:"4"},
            {id:"1"},
            {id:"2"},
        ])
    });

    it('Не найден id', () => {
        expect(sortStory(enterData, ["8"])).toEqual(enterData)
    });

    it('Нечисловой id', () => {
        expect(sortStory(enterData, ["error"])).toEqual(enterData)
    });
});