import Card from 'UI/Card';
import {Link} from "react-router-dom";
import React from "react";
import ProxyIcons from 'UI/ProxyIcons';

import Label from 'UI/Label';

export const sortStory = (stories, readStories) => {
    let renderStores = stories;

    if(readStories.length !== 0 || readStories.length !== 0 && readStories.length !== renderStores.length) {
        let mass = [];

        renderStores.map((i) => {
            if(!readStories.includes(i.id)) {
                mass.push(i)
            }
        });

        renderStores = Array.from(new Set([...mass, ...renderStores]))
    }

    return renderStores;
};

export const renderStoryItem = ({id, logicErrors, header, title}, isRead) => {
    return (
        <Link className='story-card' to={`/books/${id}`}>
            <Card className={isRead && `card--visited`}>
                {isRead && <Label style={{position: 'absolute', padding: 0}}>Просмотрено</Label>}
                {(!!logicErrors.length || isRead) && <div className="logic-errors">
                    {logicErrors.map(item => (
                        <ProxyIcons
                            src={item}
                            className='logic-error-item'
                        />
                    ))}
                </div>}
                <h3 style={{paddingBottom: 10}}>{header}</h3>
                <span className='ellipsis'>{title}</span>
                <span className="read-more">Читать &rarr;</span>
            </Card>
        </Link>
    )
}