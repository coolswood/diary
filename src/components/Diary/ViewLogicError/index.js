import React, {Component} from 'react';

import { connect } from "react-redux";

import Page from 'UI/Page';
import Header from 'UI/Header';
import Add from 'UI/Add';
import Label from 'UI/Label';
import Card from 'UI/Card';
import {Link} from "react-router-dom";

import AddIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';

import {diaryDelete} from "redux/actions/DiaryAction";

import { errors, mood as moodPic } from 'constants/index';
import {mood} from 'constants/diary';

import './style.sass'
import Badge from 'UI/Badge';
import ProxyIcons from 'UI/ProxyIcons';

class EditMind extends Component {

    diaryDelete = (history, id) => {
        history.goBack();
        this.props.diaryDelete(id);
    };

    render() {
        const { diaryList, match, history } = this.props;

        let item = diaryList.find(item => ((':' + item.id) === match.params.id));

        if(!item) return <div />;

        return (
            <div className='edit-mind'>
                <Header
                    h1='Добавить логическую ошибку'
                    {...this.props}
                />
                <Page>
                    {item.event && <>
                        <Label>Событие / ситуация / триггер</Label>
                        <Card>
                            {item.event}
                        </Card>
                    </>}
                    <Label>Автоматические мысли</Label>
                    <Card>
                        {item.logicError}
                    </Card>
                    {item.errorsSelected.length !== 0 && <>
                        <Label>Когнитивные искажения</Label>
                        <Card>
                            {item.errorsSelected.map(i => {
                                return (<div key={i} className='logic-error-wrap'>
                                    <ProxyIcons
                                        src={i}
                                        className='logic-error-item'
                                    />
                                    {errors.find(item => (item.id === i)).text}
                                </div>)
                            })}
                        </Card>
                    </>}
                    {(item.moodSelected && item.moodSelected.length !== 0) && <>
                        <Label>Эмоции</Label>
                        <Card>
                            {item.moodSelected.map(i => {
                                let selectedMood = moodPic.find(item => (item.id === i));

                                return (<div key={i} className='logic-error-wrap'>
                                    <span className='logic-error-item'>
                                        {selectedMood.moji}
                                    </span>
                                    {selectedMood.text}
                                </div>)
                            })}
                        </Card>
                    </>}
                    {!!item.moodRange && <>
                        <Label>Уровень эмоционального расстройства</Label>
                        <Card>
                            <div className='show-mood'>
                                <Badge size='small' className={`mood-badge_${item.moodRange}`} style={{position: 'static', marginRight: 10}} />
                                {mood[item.moodRange].name}
                            </div>
                        </Card>
                    </>}
                    <Label>Адекватная оценка</Label>
                    <Card>
                        {item.solution}
                    </Card>
                    {item.conclusion && <>
                        <Label>Вывод</Label>
                        <Card>
                            {item.conclusion}
                        </Card>
                    </>}
                </Page>
                <Link to={'/diary-list/edit' + match.params.id}>
                    <Add><AddIcon /></Add>
                </Link>
                    <Add onClick={() => this.diaryDelete(history, item.id)} className='add add--red' style={{left: 20, width: 60}}><DeleteIcon /></Add>
            </div>
        )
    }
}

const mapStateToProps = state => {
    const { DiaryReducer } = state;
    return {diaryList: DiaryReducer.diaryList}
};

export default connect(mapStateToProps, {diaryDelete})(EditMind);