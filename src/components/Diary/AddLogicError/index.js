import React, {Component} from 'react';

import { connect } from "react-redux";
import { DateTime } from 'luxon';

import Page from 'UI/Page';
import Header from 'UI/Header';
import TextField from 'UI/TextField';
import ErrorsCheckbox from 'UI/MultipleSelect';
import InfoErrors from 'constants/info/errors'
import Range from 'UI/RangeMood';
import {diaryEdit, diaryAdd} from "redux/actions/DiaryAction";
import SaveIcon from '@material-ui/icons/Save';
import Add from "UI/Add";

import {Link} from "react-router-dom";

import { errors, mood } from 'constants/index'

import './style.sass'
import SettingsIcon from '@material-ui/icons/Settings';
import {getLocalStore, setLocalStore} from "instruments";

class EditMind extends Component {

    constructor(props) {
        super(props);

        this.settings = getLocalStore('settings').logicErrorShowFields;
    }

    state = {
        logicError: '',
        solution: '',
        event: '',
        conclusion: '',
        errorsSelected: [],
        moodSelected: [],
        moodRange: 0,
        typeError: 'Выберите вашу ошибку',
        dt: DateTime.local(),
        edit: false,

        logicErrorWarn: false,
        solutionWarn: false
    };

    selectError = (id) => {
        const { errorsSelected } = this.state;

        let index = errorsSelected.indexOf(id);

        if(index === -1) {
            this.setState({
                errorsSelected: [...errorsSelected, id]
            })
        } else {
            let newErrorsSelected = [...errorsSelected];
            newErrorsSelected.splice(index, 1);

            this.setState({
                errorsSelected: newErrorsSelected
            })
        }
    };

    selectMood = (id) => {
        let index = this.state.moodSelected.indexOf(id);

        if(index === -1) {
            this.setState({
                moodSelected: [...this.state.moodSelected, id]
            })
        } else {
            let newMoodSelected = [...this.state.moodSelected];
            newMoodSelected.splice(index, 1);

            this.setState({
                moodSelected: newMoodSelected
            })
        }
    };

    componentDidMount() {
        const { diaryList, match } = this.props;

        // Запомимаем введенное в поля чтобы не потерять при переходе по страницам
        let storage = getLocalStore('addError');

        if(storage !== null) {

            this.setState({
                logicError: storage.logicError,
                event: storage.event,
                conclusion: storage.conclusion,
                solution: storage.solution,
                errorsSelected: storage.errorsSelected,
                moodSelected: storage.moodSelected,
                moodRange: storage.moodRange,
                edit: storage.edit
            });

            return true
        }

        if(match.params.id) {
            let item = diaryList.find(item => {
                return item.id === + match.params.id.replace(':', '')
            });

            if(!!item) {

                this.setState({
                    logicError: item.logicError,
                    event: item.event,
                    conclusion: item.conclusion,
                    solution: item.solution,
                    errorsSelected: item.errorsSelected,
                    moodSelected: item.moodSelected,
                    moodRange: item.moodRange,
                    edit: true
                })
            }
        }
    }

    componentWillUnmount() {
        setLocalStore('addError', this.state);
    }

    tipeText = (type, text) => {
        this.setState({
            [type]: text,
            logicErrorWarn: false,
            solutionWarn: false
        })
    };

    changeMoodRange = (val) => {
        this.setState({
            moodRange: val
        })
    };

    submit = (e) => {
        const { logicError, event, conclusion, solution, edit, dt, errorsSelected, moodSelected, moodRange } = this.state;
        const { diaryEdit, diaryAdd, history, match, diaryList } = this.props;
        e.preventDefault();

        if(logicError === '') {
            this.setState({
                logicErrorWarn: true
            })
        }

        if(solution === '') {
            this.setState({
                solutionWarn: true
            })
        }

        if(logicError === '' || solution === '') return;

        if(!!edit) {
            let item = diaryList.find(item => {
                if( ':' + item.id === match.params.id) {
                    return true
                }
            });

            item.logicError = logicError;
            item.event = event;
            item.conclusion = conclusion;
            item.solution = solution;
            item.moodRange = moodRange;
            item.errorsSelected = errorsSelected;
            item.moodSelected = moodSelected;

            diaryEdit(item);
        } else {
            diaryAdd({id: dt.ts,
                logicError: logicError,
                event: event,
                conclusion: conclusion,
                solution: solution,
                errorsSelected: errorsSelected,
                moodSelected: moodSelected,
                moodRange: moodRange,
                dt: dt.setLocale('ru').toLocaleString(DateTime.DATE_FULL)});
            }

        history.goBack()
    };

    render() {
        const { logicError, event, conclusion, solution, errorsSelected, moodSelected, moodRange, logicErrorWarn, solutionWarn } = this.state;

        return (
            <div className='edit-mind'>
                <Header
                    h1='Добавить автоматическую мысль'
                    {...this.props}
                />
                <Page>
                    <form>
                        {this.settings.event && <TextField
                            className='field-event'
                            tipeText={this.tipeText}
                            value={event}
                            type='event'
                            label="Событие / ситуация / триггер"
                        />}
                        {this.settings.logicError && <TextField
                            className='field-logicError'
                            tipeText={this.tipeText}
                            error={logicErrorWarn}
                            value={logicError}
                            type='logicError'
                            label="Автоматические мысли"
                        />}
                        {this.settings.errorsSelected && <ErrorsCheckbox
                            className='offset-top logic-error'
                            title="Выберите логическую ошибку"
                            content={errors}
                            selected={errorsSelected}
                            select={this.selectError}
                            info={<InfoErrors />}
                        />}
                        {this.settings.moodSelected && <ErrorsCheckbox
                            className='offset-top emotion'
                            title="Выберите вашу эмоцию"
                            content={mood}
                            selected={moodSelected}
                            select={this.selectMood}
                        />}
                        {this.settings.moodRange && <Range
                            value={moodRange}
                            changeRange={this.changeMoodRange}
                        />}
                        {this.settings.solution && <TextField
                            className='field-solution'
                            tipeText={this.tipeText}
                            error={solutionWarn}
                            value={solution}
                            type='solution'
                            label="Адекватная оценка"
                        />}
                        {this.settings.conclusion && <TextField
                            className='field-conclusion'
                            tipeText={this.tipeText}
                            value={conclusion}
                            type='conclusion'
                            label="Вывод"
                        />}
                        <Add onClick={this.submit}><SaveIcon /></Add>
                        <Link to='/settings'><Add className='add add--red' style={{left: 20, width: 60}}><SettingsIcon /></Add></Link>
                    </form>
                </Page>
            </div>
        )
    }
}

const mapStateToProps = state => {
    const { DiaryReducer } = state;
    return {diaryList: DiaryReducer.diaryList}
};

export default connect(mapStateToProps, {diaryEdit, diaryAdd})(EditMind);