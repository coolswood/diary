import React, {Component} from 'react';

import { connect } from "react-redux";

import Page from 'UI/Page';
import Header from 'UI/Header';
import Add from 'UI/Add';
import Label from 'UI/Label';
import Alert from 'UI/Alert';
import Card from 'UI/Card';
import Badge from 'UI/Badge';
import ProxyIcons from 'UI/ProxyIcons';

import {Link} from "react-router-dom";

import AddIcon from '@material-ui/icons/Add';

import './style.sass'

class JournalErrors extends Component {

    constructor(props) {
        super(props);

        this.date = 0;
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if(nextProps.diaryList.length !== this.props.diaryList.length) {
            this.date = 0;
        }
    }

    componentWillUnmount() {
        localStorage.setItem('addError', null);
    }

    render() {
        const { match, diaryList, location } = this.props;

        return (
            <div className='journal-errors'>
                <Header
                    h1='Дневник автоматических мыслей'
                    {...this.props}
                />
                <Page>
                    <h2>{match.params.name}</h2>
                    {diaryList.length !== 0 ? [...diaryList].reverse().map((item, id) => {

                        let date = item.dt;
                        let showDate = true;

                        if(this.date !== date) {
                            this.date = date;
                            showDate = true;
                        } else {
                            showDate = false;
                        }

                        return (
                        <div key={item.id}>
                           {showDate && <Label>{date}</Label>}
                            <Link to={`${location.pathname}${'/view:' + item.id}`}>
                                <Card>
                                    {!!item.moodRange && <Badge size='small' className={`mood-badge_${item.moodRange}`} style={{top: 7, right: 7}} />}
                                    {(item.errorsSelected && (!!item.errorsSelected.length || !!item.moodRange)) && <div className="logic-errors">
                                        {item.errorsSelected.map(i => (
                                            <ProxyIcons
                                                src={i}
                                                className='logic-error-item'
                                            />
                                        ))}
                                    </div>}
                                    <div>
                                        <span className='ellipsis'>{item.logicError}</span>
                                        <div className="hr" style={{
                                            margin: '15px auto',
                                            height: 1,
                                            background: '#80808061',
                                        }} />
                                        <span className='ellipsis'>{item.solution}</span>
                                    </div>
                                </Card>
                            </Link>
                        </div>)
                    }) : <Card>
                        <Alert
                            type='empty'
                        />
                    </Card>}
                </Page>
                <Link to={location.pathname + '/add'}>
                    <Add><AddIcon /></Add>
                </Link>
            </div>
        )
    }
}

const mapStateToProps = state => {
    const { DiaryReducer } = state;
    return DiaryReducer
};

export default connect(mapStateToProps)(JournalErrors);