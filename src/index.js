import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter } from "react-router-dom";
import './style.sass';

import { Provider } from "react-redux";
import store from "./redux/store";
import * as Sentry from "@sentry/browser";
import {isProd} from "instruments";

if(isProd()) {
    Sentry.init({dsn: "https://bcf7ddac582d4f0b9713bd17141f19af@sentry.io/1774745"});
}

ReactDOM.render(
    <BrowserRouter>
        <Provider store={store}>
            <App />
        </Provider>
    </BrowserRouter>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
