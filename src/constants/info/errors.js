import React, {Component} from 'react';
import {errors} from "../index";
import Collapse from '@material-ui/core/Collapse';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from "@material-ui/core/ListItem";
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ProxyIcons from 'UI/ProxyIcons';

export default class Info extends Component {

    state = {
      opened: ''
    };

    handleClick = (id) => {
        this.setState({
            opened: id === this.state.opened ? '' : id
        })
    };

    render() {
        return (
            <div>
                {errors.map(item => {
                    return (
                        <div key={item.id} style={{marginBottom: 10}} className='errors-checkbox__select'>
                            <ListItem button onClick={() => this.handleClick(item.id)}>
                                {ProxyIcons(item.id, 'logic-error-item')}
                                <ListItemText primary={item.text} />
                                {this.state.open ? <ExpandLess /> : <ExpandMore />}
                            </ListItem>
                            <Collapse in={this.state.opened === item.id} timeout="auto">
                                <div>
                                    {item.disc}
                                </div>
                            </Collapse>
                        </div>
                    )
                })}
            </div>
        )
    }
}