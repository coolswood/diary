import React from 'react';

import Card from '../../UI/Card';

export default () => (
    <Card>
        На этой странице вы можете пройти тесты, которые помогут вам найти свои слабые стороны или проследить прогресс.
    </Card>
)