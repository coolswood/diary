import React from 'react';

import Label from 'UI/Label';
import Card from 'UI/Card';
import CardNewVersion from 'UI/CardNewVersion';
import {VERSION} from "../../index";

export default () => {
    localStorage.setItem('version', VERSION);

    return (
        <>
            <Label>В новой версии добавлено</Label>
            <CardNewVersion
                link={'/login'}
            >
                Теперь можно устанавливать пароль для входа в приложение. Никто не сможет взять ваш телефон и прочитать личные записи.
            </CardNewVersion>

            <Label>Оцените нас</Label>
            <a href="https://play.google.com/store/apps/details?id=com.coffye.mindhealthy">
                <Card>
                    <img className='new-version__img' src={require('img/new-version/google.png')} alt=""/>
                    <h3 style={{textAlign: 'center', fontSize: 23, margin: 15}}>Будьте здоровы и хорошего дня!</h3>
                    <img className='new-version__img' src={require(`img/new-version/stars.jpg`)} alt=""/>
                </Card>
            </a>

        </>
    )
}