import React, {Component} from 'react';

import Card from 'UI/Card';
import Label from 'UI/Label';
import HDUErrorsList from 'UI/HDUErrorsList'

export default class StatsError extends Component {

    render() {
        return (
            <>
                <Label>BDI тест</Label>
                <Card>
                    BDI тест покажет на сколько высок ваш уровень депрессивного расстройства.
                </Card>
                <Label>Шкала дисфункциональных убеждений</Label>
                <Card>
                    Шкала дисфункциональных убеждений поможет точечно определить в каких сферах вашего восприятия мира
                    присутствуют когнитивные искажения.
                </Card>
                <HDUErrorsList />
            </>
        )
    }
};