export const BDI_START_DATA = 'BDI_START_DATA';
export const BDI_ADD = 'BDI_ADD';

export const GET_ALL_DATA = 'GET_ALL_DATA';
export const ADD_LOGIC_ERROR = 'ADD_LOGIC_ERROR';
export const EDIT_LOGIC_ERROR = 'EDIT_LOGIC_ERROR';
export const DIARY_DELETE = 'DIARY_DELETE';

export const HDU_START_DATA = 'HDU_START_DATA';
export const HDUAddData = 'HDUAddData';