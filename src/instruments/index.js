export const isProd = () => (
    window.location.hostname === 'mind-healthy.ru'
);

export const getLocalStore = (val) => (
    JSON.parse(localStorage.getItem(val))
);

export const setLocalStore = (name, val) => (
    localStorage.setItem(name, JSON.stringify(val))
);

export const ApiPost = (url, data) => (
    fetch(url, {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: 'POST',
        body: JSON.stringify(data)
    }).then(response => response.json())
);