import { combineReducers } from "redux";

import DiaryReducer from './DiaryReducer'
import BDIReducer from './BDIReducer'
import HDUReducer from './HDUReducer'

export default combineReducers({
    DiaryReducer,
    BDIReducer,
    HDUReducer
});