import BDIReducer, {initialState} from "redux/reducers/BDIReducer";
import {BDI_ADD, BDI_START_DATA} from "constants/actionTypes";

describe('BDIReducer', () => {

    it(BDI_START_DATA, () => {
        const action = {
            type: BDI_START_DATA,
            payload: []
        };

        expect(BDIReducer(initialState, action)).toEqual({
            ...initialState,
            BDIAll: action.payload
        })
    })

    it(BDI_ADD, () => {

        const action = {
            type: BDI_ADD,
            payload: {
                date: "28 сентября 2019 г.",
                id: 1569660687198,
                result: (21) [1, 2, 0, 2, 1, 2, 1, 1, 0, 0, 0, 1, 1, 0, 2, 0, 2, 1, 2, 0, 1],
                score: 20
            }
        };

        expect(BDIReducer(initialState, action)).toEqual({
            ...initialState,
            BDIAll: [action.payload]
        })
    })
});