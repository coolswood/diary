import DiaryReducer, {initialState} from "redux/reducers/DiaryReducer";
import {ADD_LOGIC_ERROR, DIARY_DELETE, EDIT_LOGIC_ERROR, GET_ALL_DATA} from "constants/actionTypes";

const logicErrorFields = {
    conclusion: "",
    dt: "28 сентября 2019 г.",
    errorsSelected: ["max"],
    event: "",
    id: 1569671934981,
    logicError: "",
    moodRange: "2",
    moodSelected: ["anger"],
    solution: ""
};

describe('DiaryReducer', () => {

    it(GET_ALL_DATA, () => {
        const action = {
            type: GET_ALL_DATA,
            payload: []
        };

        expect(DiaryReducer(initialState, action)).toEqual({
            ...initialState,
            diaryList: action.payload
        })
    })

    it(ADD_LOGIC_ERROR, () => {
        const action = {
            type: ADD_LOGIC_ERROR,
            payload: logicErrorFields
        };

        expect(DiaryReducer(initialState, action)).toEqual({
            ...initialState,
            diaryList: [...initialState.diaryList, action.payload]
        })
    })

    it(EDIT_LOGIC_ERROR, () => {
        const initialBefore = {
            diaryList: [
                {...logicErrorFields, id: 1},
                logicErrorFields,
            ]
        };

        const action = {
            type: EDIT_LOGIC_ERROR,
            payload: {...logicErrorFields, conclusion: "test"}
        };

        expect(DiaryReducer(initialBefore, action)).toEqual({
            ...initialState,
            diaryList: [
                {...logicErrorFields, id: 1},
                {...logicErrorFields, conclusion: "test"}
            ]
        })
    })

    it(DIARY_DELETE, () => {
        const initialBefore = {
            diaryList: [
                {...logicErrorFields, id: 1},
                logicErrorFields,
            ]
        };

        const action = {
            type: DIARY_DELETE,
            payload: 1
        };

        expect(DiaryReducer(initialBefore, action)).toEqual({
            ...initialState,
            diaryList: [
                logicErrorFields
            ]
        })
    })
})