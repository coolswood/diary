import HDUReducer, {initialState} from "redux/reducers/HDUReducer";
import {BDI_START_DATA, HDU_START_DATA, HDUAddData} from "constants/actionTypes";
import {DateTime} from "luxon";

describe('HDUReducer', () => {

    it(HDU_START_DATA, () => {
        const action = {
            type: HDU_START_DATA,
            payload: [{
                date: "28 сентября 2019 г.",
                id: 1569698204864,
                result: [-2, 2, 2, -3, 1, -3, -4],
                steps: [-2, 1, -2, 0, 1, 1, 2, -1, -1, 1, 2, -1, 1, -1, 1, -1, 1, -2, 0, -1, 0, 0, 1, -1, 1, -1, 0, -1, -2, 1, -1, 0, 0, -2, -1]
            }]
        };

        expect(HDUReducer(initialState, action)).toEqual({
            ...initialState,
            HDUAll: action.payload
        })
    })

    it(HDUAddData, () => {
        const action = {
            type: HDUAddData,
            payload: {
                id: 1569618000000,
                date: "September 28, 2019",
                result: [0, -4, -1, -1, -1, -2, -3],
                steps: [-1, 0, -2, 1, 0, 0, -1, 1, 0, 1, 1, 0, -1, -1, 0, 2, 2, 1, 0, -1, -1, 1, 1, -1, 1, 2, -1, -1, 0, 2, 0, 0, 1, -1, 0]
            }
        };

        expect(HDUReducer(initialState, action)).toEqual({
            ...initialState,
            HDUAll: [...initialState.HDUAll, {
                id: 1569618000000,
                result: [0, -4, -1, -1, -1, -2, -3],
                date: "September 28, 2019",
                steps: [-1, 0, -2, 1, 0, 0, -1, 1, 0, 1, 1, 0, -1, -1, 0, 2, 2, 1, 0, -1, -1, 1, 1, -1, 1, 2, -1, -1, 0, 2, 0, 0, 1, -1, 0]
            }]
        })
    })

});