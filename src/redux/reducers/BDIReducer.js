import {BDI_ADD, BDI_START_DATA} from "constants/actionTypes";

export const initialState = {
    BDIAll: []
};

    export default function(state = initialState, action) {
    switch (action.type) {
        case BDI_START_DATA: {
            return {
                ...state,
                BDIAll: action.payload
            };
        }
        case BDI_ADD: {

            return {
                ...state,
                BDIAll: [...state.BDIAll, action.payload]
            };
        }
        default:
            return state;
    }
}