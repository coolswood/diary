import {HDU_START_DATA, HDUAddData} from "constants/actionTypes";

export const initialState = {
    HDUAll: []
};

    export default function(state = initialState, action) {
    switch (action.type) {
        case HDU_START_DATA: {

            return {
                ...state,
                HDUAll: action.payload
            };
        }
        case HDUAddData: {

            return {
                ...state,
                HDUAll: [...state.HDUAll, action.payload]
            };
        }
        default:
            return state;
    }
}