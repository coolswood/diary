import findIndex from 'lodash/findIndex';
import remove from 'lodash/remove';
import {ADD_LOGIC_ERROR, DIARY_DELETE, EDIT_LOGIC_ERROR, GET_ALL_DATA} from "constants/actionTypes";

export const initialState = {
    diaryList: []
};

export default function(state = initialState, action) {
    switch (action.type) {
        case GET_ALL_DATA: {

            return {
                ...state,
                diaryList: action.payload
            };
        }

        case ADD_LOGIC_ERROR: {

            return {
                ...state,
                diaryList: [...state.diaryList, action.payload]
            };
        }

        case EDIT_LOGIC_ERROR: {

            let index = findIndex(state.diaryList, {id: action.payload.id});

            if(index === -1) {
                return {
                    ...state,
                    diaryList: state.diaryList
                };
            }

            let newArr = [...state.diaryList];

            newArr.splice(index, 1, action.payload);

            return {
                ...state,
                diaryList: newArr
            };
        }

        case DIARY_DELETE: {

            let newArr = [...state.diaryList];

            remove(newArr, (i) => (i.id === action.payload));

            return {
                ...state,
                diaryList: newArr
            };
        }

        default:
            return state;
    }
}