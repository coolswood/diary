import {addDiary, deleteDiary} from '../../api'
import {ADD_LOGIC_ERROR, DIARY_DELETE, EDIT_LOGIC_ERROR, GET_ALL_DATA} from "constants/actionTypes";
import {editDiary} from "api";

export const getAllData = (content) => ({
    type: GET_ALL_DATA,
    payload: content
});

export const diaryAdd = (content) => {

    addDiary(content);

    return {
        type: ADD_LOGIC_ERROR,
        payload: content
    }
};

export const diaryEdit = (content) => {

    editDiary(content)

    return {
        type: EDIT_LOGIC_ERROR,
        payload: content
    }
};

export const diaryDelete = (id) => {
    deleteDiary(id);

    return {
        type: DIARY_DELETE,
        payload: id
    }
};