import {addHDU} from '../../api'
import {DateTime} from "luxon";
import {HDU_START_DATA, HDUAddData} from "constants/actionTypes";

export const HDUStartData = (content) => {

    return {
        type: HDU_START_DATA,
        payload: content
    }
};

export const HDUAdd = (score, steps, time) => {

    const formatTime = time.toLocaleString(DateTime.DATE_FULL);

    const data = {
        id: time.ts,
        date: formatTime,
        result: score,
        steps
    };

    addHDU(data);

    return {
        type: HDUAddData,
        payload: data
    }
};