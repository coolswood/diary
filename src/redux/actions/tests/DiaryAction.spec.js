import {ADD_LOGIC_ERROR, DIARY_DELETE, EDIT_LOGIC_ERROR, GET_ALL_DATA} from "constants/actionTypes";
import {diaryAdd, diaryDelete, diaryEdit, getAllData} from "redux/actions/DiaryAction";

describe('DiaryAction', () => {

    const logicErrorFields = {
        conclusion: "",
        dt: "28 сентября 2019 г.",
        errorsSelected: ["max"],
        event: "",
        id: 1569671934981,
        logicError: "",
        moodRange: "2",
        moodSelected: ["anger"],
        solution: ""
    };

    it('getAllData', () => {

        expect(getAllData([])).toEqual({
            type: GET_ALL_DATA,
            payload: []
        })
    });

    it('diaryAdd', () => {

        expect(diaryAdd(logicErrorFields)).toEqual({
            type: ADD_LOGIC_ERROR,
            payload: logicErrorFields
        })
    });

    it('diaryEdit', () => {

        expect(diaryEdit(logicErrorFields)).toEqual({
            type: EDIT_LOGIC_ERROR,
            payload: logicErrorFields
        })
    })

    it('diaryDelete', () => {

        expect(diaryDelete(1)).toEqual({
            type: DIARY_DELETE,
            payload: 1
        })
    })
});