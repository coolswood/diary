import {BDI_ADD, BDI_START_DATA} from "constants/actionTypes";
import {BDIAdd, BDIStartData} from "redux/actions/BDIAction";
import {DateTime} from "luxon";

describe('BDIAction', () => {

    it('BDIStartData', () => {

        expect(BDIStartData([])).toEqual({
            type: BDI_START_DATA,
            payload: []
        })
    });

    it('BDIAdd', () => {
        const data = {
            result: [2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1],
            time: DateTime.local(2019, 9, 28)
        };

        expect(BDIAdd(data.result, data.time)).toEqual({
            type: BDI_ADD,
            payload: {
                date: "September 28, 2019",
                id: 1569618000000,
                result: [2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1],
                score: 21
            }
        })
    })
});