import {HDU_START_DATA, HDUAddData} from "constants/actionTypes";
import {HDUAdd, HDUStartData} from "redux/actions/HDUAction";
import {DateTime} from "luxon";

describe('HDUAction', () => {

    it('HDUStartData', () => {

        expect(HDUStartData([])).toEqual({
            type: HDU_START_DATA,
            payload: []
        })
    });

    it('HDUAdd', () => {
        const data = {
            score: [0, -4, -1, -1, -1, -2, -3],
            steps: [-1, 0, -2, 1, 0, 0, -1, 1, 0, 1, 1, 0, -1, -1, 0, 2, 2, 1, 0, -1, -1, 1, 1, -1, 1, 2, -1, -1, 0, 2, 0, 0, 1, -1, 0],
            time: DateTime.local(2019, 9, 28)
        };

        expect(HDUAdd(data.score, data.steps, data.time)).toEqual({
            type: HDUAddData,
            payload: {
                id: 1569618000000,
                date: "September 28, 2019",
                result: [0, -4, -1, -1, -1, -2, -3],
                steps: [-1, 0, -2, 1, 0, 0, -1, 1, 0, 1, 1, 0, -1, -1, 0, 2, 2, 1, 0, -1, -1, 1, 1, -1, 1, 2, -1, -1, 0, 2, 0, 0, 1, -1, 0]
            }
        })
    });

});