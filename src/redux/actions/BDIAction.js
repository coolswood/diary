import {addBDI} from '../../api'
import {DateTime} from "luxon";
import {BDI_ADD, BDI_START_DATA} from "constants/actionTypes";

export const BDIStartData = (content) => ({
    type: BDI_START_DATA,
    payload: content
});

export const BDIAdd = (result, time) => {
    let score = result.reduce((res, i) => (
       res += i
    ), 0);
    let formatTime = time.toLocaleString(DateTime.DATE_FULL);

    const data = {
        id: time.ts,
        date: formatTime,
        result,
        score
    };

    addBDI(data);

    return {
        type: BDI_ADD,
        payload: data
    }
};