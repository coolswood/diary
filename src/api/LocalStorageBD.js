import {getLocalStore, setLocalStore} from "instruments";

export const getDiaryOld = () => {
    let storeData = getLocalStore('diaryOld');

    if(storeData === null) {
        setLocalStore('diaryOld', []);
        return []
    }

    return storeData
};

export const addDiaryOld = (data) => {
    let storeData = getLocalStore('diaryOld');

    if(storeData === null) {
        setLocalStore('diaryOld', [data])
    } else {
        let newData = [...storeData];

        newData.push(data);

        setLocalStore('diaryOld', newData)
    }
};

export const editDiaryOld = (content) => {
    let storeData = getLocalStore('diaryOld');

    let filtered = storeData.filter(i => i.id !== content.id);
    filtered.push(content);

    setLocalStore('diaryOld', filtered)
}

export const deleteDiaryOld = (id) => {
    let storeData = getLocalStore('diaryOld');

    let filtered = storeData.filter(i => i.id !== id);

    setLocalStore('diaryOld', filtered)
}

export const getBDIDataOld = () => {
    let storeData = getLocalStore('BDIDataOld');

    if(storeData === null) {
        setLocalStore('BDIDataOld', []);
        return []
    }

    return storeData
}

export const addBDIOld = (data) => {
    let storeData = getLocalStore('BDIDataOld');

    if(storeData === null) {
        setLocalStore('BDIDataOld', [data])
    } else {
        let newData = [...storeData];

        newData.push(data);

        setLocalStore('BDIDataOld', newData)
    }
}

export const getHDUDataOld = () => {
    let storeData = getLocalStore('HDUDataOld');

    if(storeData === null) {
        setLocalStore('HDUDataOld', []);
        return []
    }

    return storeData
}

export const addHDUOld = (data) => {
    let storeData = getLocalStore('HDUDataOld');

    if(storeData === null) {
        setLocalStore('HDUDataOld', [data])
    } else {
        let newData = [...storeData];

        newData.push(data);

        setLocalStore('HDUDataOld', newData)
    }
}