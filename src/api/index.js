import { openDB, deleteDB } from 'idb';
import {
    addBDIOld,
    addDiaryOld,
    addHDUOld,
    deleteDiaryOld, editDiaryOld,
    getBDIDataOld,
    getDiaryOld,
    getHDUDataOld
} from "api/LocalStorageBD";
import {getLocalStore, setLocalStore} from "instruments";

let db = '';

export async function doDatabaseStuff() {
    db = await openDB('PSY', 2, {
        upgrade(res, oldVersion) {
            switch (oldVersion) {
                case 1:
                    res.createObjectStore('hdu', {
                        keyPath: 'id',
                        autoIncrement: true
                    });
                    break;
                default:
                    res.createObjectStore('diary', {
                        keyPath: 'id',
                        autoIncrement: true
                    });

                    res.createObjectStore('bdi', {
                        keyPath: 'id',
                        autoIncrement: true
                    });

                    res.createObjectStore('hdu', {
                        keyPath: 'id',
                        autoIncrement: true
                    });
            }
        },
    });

    return true
}

export async function getAllData() {

    if(!db) {
        let diaryOld = getLocalStore('diaryOld');
        let BDIDataOld = getLocalStore('BDIDataOld');
        let HDUDataOld = getLocalStore('HDUDataOld');

        return {
            diary: diaryOld,
            bdi: BDIDataOld,
            hdu: HDUDataOld
        }
    }

    let diary = await db.getAll('diary');
    let bdi = await db.getAll('bdi');
    let hdu = await db.getAll('hdu');

    return {
        diary,
        bdi,
        hdu
    }
}

export async function restoreAllData(data) {

    if(!db) {
        setLocalStore('diaryOld', data.diary);
        setLocalStore('BDIDataOld', data.bdi);
        setLocalStore('HDUDataOld', data.hdu);

        return;
    }

    const txDiary = db.transaction('diary', 'readwrite');

    data.diary.forEach(i => {
        txDiary.store.put(i);
    });

    const txBdi = db.transaction('bdi', 'readwrite');
    data.bdi.forEach(i => {
        txBdi.store.put(i);
    });

    const txHdu = db.transaction('hdu', 'readwrite');
    data.hdu.forEach(i => {
        txHdu.store.put(i);
    })
}

export async function getDiary() {
    if(!db) {
        return getDiaryOld()
    }

    return await db.getAll('diary');
}

export function editDiary(content) {
    if(!db) {
        return editDiaryOld(content)
    }

    const tx = db.transaction('diary', 'readwrite');
    tx.store.put(content);
};

export function addDiary(content) {
    let data = {
        id: content.id,
        dt: content.dt,
        moodRange: content.moodRange,
        errorsSelected: content.errorsSelected,
        moodSelected: content.moodSelected,
        logicError: content.logicError,
        conclusion: content.conclusion,
        event: content.event,
        solution: content.solution,
    };

    if(!db) {
        return addDiaryOld(data)
    }

    const tx = db.transaction('diary', 'readwrite');
    tx.store.add(data);
}

export function deleteDiary(id) {
    if(!db) {
        return deleteDiaryOld(id)
    }

    const tx = db.transaction('diary', 'readwrite');
    tx.store.delete(id);
}

export async function getBDIData() {
    if(!db) {
        return getBDIDataOld()
    }

    return await db.getAll('bdi');
};

export function addBDI(data) {
    if(!db) {
        return addBDIOld(data)
    }

    const tx = db.transaction('bdi', 'readwrite');
    tx.store.add(data);
};

export async function getHDUData() {
    if(!db) {
        return getHDUDataOld()
    }

    return await db.getAll('hdu');
};

export function addHDU(data) {
    if(!db) {
        return addHDUOld(data)
    }

    const tx = db.transaction('hdu', 'readwrite');

    tx.store.add(data);
};
