const express = require('express');
const bodyParser = require('body-parser');
const bcrypt = require('bcryptjs');
const generator = require('random-password');
const app = express();
const mail = require('./mail');

const db = require("./db");
db.openDB();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

const salt = bcrypt.genSaltSync(10);

app.get('/*',function(req,res){
    res.sendFile(__dirname + '/index.html');
});

app.post('/api/register', (req, res) => {
    const {login, password} = req.body;

    try {
        const userData = db.getData(login);
        const userPassword = userData.password;

        if(bcrypt.compareSync(password, userPassword)) {
            return res.status(200).send({
                status: "OK",
                data: req.body
            });
        }

        return res.status(400).send({
            status: "wrongPassword",
            text: "Неправильный пароль"
        });
    } catch(error) {

        db.addDB(login, {password: bcrypt.hashSync(req.body.password, salt)});

        return res.status(200).send({
            status: "OK",
            data: req.body
        });
    }
});

app.post('/api/getPassword', (req, res) => {
    const {login} = req.body;
    const newPassword = generator(6);

    try {
        const userData = db.getData(login);

        let data = JSON.parse(JSON.stringify(userData));

        delete data.login;

        db.addDB(login, {...data, password: bcrypt.hashSync(newPassword, salt)});

        mail(login, `Ваш новый пароль: ${newPassword}`);

        return res.status(200).send({
            status: "OK",
            text: "Пароль отправлен на указанный Email"
        });
    } catch(error) {
        return res.status(400).send({
            status: "wrongLogin",
            text: "Не найден Email"
        });
    }
});

app.post('/api/backup', (req, res) => {
    const {login, lastUpdate, data} = req.body;

    try {
        db.addDB(login, {...db.getData(login), data: data, lastUpdate: lastUpdate});

        return res.send({
            status: "OK"
        })
    } catch(error) {

    }
});

app.post('/api/lastUpdate', (req, res) => {
    const {login} = req.body;

    try {
        const date = db.getData(login).lastUpdate;

        if(!date) {
            return res.send({
                status: "noData"
            })
        }

        return res.send({
            status: "OK",
            lastUpdate: date
        })
    } catch(error) {

    }
});

app.post('/api/restore', (req, res) => {
    const {login} = req.body;

    try {
        const userData = db.getData(login);

        res.send({
            status: "OK",
            data: userData
        });
    } catch(error) {

    }
});

app.listen(3001, function () {
    console.log('Example app listening on port 3000!');
});