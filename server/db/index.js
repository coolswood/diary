const JsonDB = require('node-json-db').JsonDB;
const Config = require('node-json-db/dist/lib/JsonDBConfig').Config;

let db = null;

module.exports = {
    openDB: () => {
        db = new JsonDB(new Config("database", true, false, '/'));
    },

    addDB: (login, data) => {
        db.push(`/${login}`, data);
    },

    getData: (data) => {
        return db.getData(`/${data}`);
    }
}